jQuery(document).ready(function (){
    jQuery("[data-type='upload']").each(function (){
        if(jQuery(this).data("file_name") == undefined){
            jQuery(this).append("<span>Subir archivo</span>")
        }else{
            jQuery(this).append("<span>" + jQuery(this).data("file_name") + "</span>")
        }
    })
    jQuery("[data-type='upload'] input").on("change", function (){
        let files = jQuery(this)[0].files;
        jQuery(this).parent().find("span").text(files[0].name);
    })
})
