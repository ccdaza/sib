<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class QuoteRequest extends Mailable
{
    use Queueable, SerializesModels;

    protected $provider;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($provider)
    {
        $this->provider = $provider;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $hash =  base64_encode("proveedor-" . $this->provider->id);
        return $this->from(env("MAIL_USERNAME"))->view('mail.QuoteRequest', compact("hash"));
    }
}
