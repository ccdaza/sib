<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function actions(){
        return $this->hasMany(ModuleAction::class, "module_id", "id");
    }
}
