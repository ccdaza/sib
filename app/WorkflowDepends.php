<?php

namespace App;

use App\Models\Model;

class WorkflowDepends extends Model
{

    public function task(){
        return $this->belongsTo(Workflow::class, "depends_task_id", "id");
    }
}
