<?php

namespace App;

use Illuminate\Database\Migrations\Migration as MigrationCore;

class Migration extends MigrationCore
{
    public $prefix = null;

    public function __construct($year = null)
    {
        $this->prefix = $year ?? date("Y");
        $this->prefix .= "_";
    }
}
