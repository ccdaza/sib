<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriesShopping extends Model
{
    protected $table = "categories_shopping";
}
