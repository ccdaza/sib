<?php

namespace App;

use App\Models\Model;

class Workflow extends Model
{

    public function depends(){
        return $this->hasMany(WorkflowDepends::class, "task_id", "id");
    }
}
