<?php

namespace App;

use App\Models\ActivitiesExtra;
use App\Models\Budget;
use App\Models\Model;

class Area extends Model
{

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = "areas";
    }

    public function budgets(){
        return $this->hasMany(Budget::class, "area_id", "id");
    }

    public function extras(){
        return $this->hasMany(ActivitiesExtra::class, "area_id", "id");
    }
}
