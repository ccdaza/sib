<?php

namespace App\Models;

use App\Area;
use App\Models\Model;

class ActivitiesExtra extends Model
{
    public $table = "activities_extra";
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function activities(){
        return $this->hasMany(ActivitiesExtraActivity::class, "activities_extra_id", "id");
    }

}
