<?php

namespace App\Models;

use App\Models\Model;

class Frequency extends Model
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = "frequencies";
    }
}
