<?php

namespace App\Models\Budget;

use App\Models\CenterCost;
use App\Models\Model;

class Resource extends Model
{
    protected $table = "budget_resources";

    public function centerCost(){
        return $this->belongsTo(CenterCost::class, "center_cost_id", "id");
    }
    public function activity(){
        return $this->belongsTo(Activity::class, "activity_id", "id");
    }
}
