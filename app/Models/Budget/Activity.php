<?php

namespace App\Models\Budget;

use App\Models\Budget;
use App\Models\Office;
use App\RequestMoney;
use App\User;
use App\Models\Model;

class Activity extends Model
{
    protected $table = "budget_activities";

    public function executor(){
        return $this->belongsTo(User::class, "executor_id", "id");
    }
    public function responsable(){
        return $this->belongsTo(User::class, "responsible_id", "id");
    }
    public function office(){
        return $this->belongsTo(Office::class, "office_id", "id");
    }
    public function resources(){
        return $this->hasMany(Resource::class, "activity_id", "id");
    }
    public function request(){
        return $this->belongsTo(RequestMoney::class, "id", "activity_id");
    }
    public function budget(){
        return $this->belongsTo(Budget::class, "budget_id", "id");
    }
}
