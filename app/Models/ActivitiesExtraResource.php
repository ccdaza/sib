<?php

namespace App\Models;

use App\Area;
use App\Models\Model;

class ActivitiesExtraResource extends Model
{
    public $table = "activities_extra_resources";
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

}
