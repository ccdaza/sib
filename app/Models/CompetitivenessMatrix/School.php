<?php

namespace App\Models\CompetitivenessMatrix;

use App\Models\Model;

class School extends Model
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function factorsScore(){
        return $this->hasMany(FactorSchoolScore::class, "school_id", "id");
    }
}
