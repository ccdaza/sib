<?php

namespace App\Models\CompetitivenessMatrix;

use App\Models\Dofa\Strength;
use App\Models\Dofa\Weaknesses;
use App\Models\Model;

class Factor extends Model
{
    protected $table = "competitiveness_matrix_factors";

    public function Strengths(){
        return $this->hasMany(Strength::class, "factor_id", "id");
    }

    public function Weaknesses(){
        return $this->hasMany(Weaknesses::class, "factor_id", "id");
    }
}
