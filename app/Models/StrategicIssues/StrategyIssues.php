<?php

namespace App\Models\StrategicIssues;

use App\Models\Dofa\WeaknessOpportunity;
use App\Models\ObjectivesBalancedScorecard;
use App\StrategicIssuesMatrixScore;
use App\Models\Model;
use Illuminate\Support\Facades\Auth;

class StrategyIssues extends Model
{
    public function category(){
        return $this->belongsTo(Category::class, "category_id", "id");
    }
    public function strategy(){
        return $this->belongsTo(Strategies::class, "strategy_id", "id");
    }
    public function getModelAttribute($value)
    {
        $model = new $value();
        return $model::where("id", $this->model_id)->get()->last();
    }

    public function userScore(){
        return $this->hasMany(StrategicIssuesMatrixScore::class, "data_id", "id")
                    ->where("type", "strategicIssues")
                    ->where("user_id", Auth::id());
    }
}
