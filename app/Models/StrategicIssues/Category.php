<?php

namespace App\Models\StrategicIssues;

use App\Models\Model;

class Category extends Model
{
    protected $table = "strategic_issues_categories";

    public function strategicIssues(){
        return $this->hasMany(StrategyIssues::class, "category_id", "id");
    }

    public function objectives(){
        return $this->hasMany(Objective::class, "category_id", "id");
    }
}
