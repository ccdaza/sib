<?php

namespace App\Models\StrategicIssues;

use App\Models\Model;

class Strategies extends Model
{
    protected $table = "strategic_issues_strategies";
}
