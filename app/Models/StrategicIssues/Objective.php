<?php

namespace App\Models\StrategicIssues;

use App\Models\ObjectivesBalancedScorecard;
use App\Models\Model;

class Objective extends Model
{
    protected $table = "strategic_issues_categories_objectives";

    public function category(){
        return $this->belongsTo(Category::class, "category_id", "id");
    }

}
