<?php

namespace App\Models\StrategicIssues;

use App\Models\ObjectivesBalancedScorecard;
use App\Models\Model;

class Perspective extends Model
{


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = "strategic_issues_perspectives";
    }

    public function objectives(){
        return $this->hasMany(Objective::class, "perspective_id", "id");
    }
}
