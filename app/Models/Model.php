<?php

namespace App\Models;

use Illuminate\Support\Facades\Session;

class Model extends \Illuminate\Database\Eloquent\Model{


    public function __construct(array $attributes = [])
    {
        $this->table = Session::get("year", date("Y")) . "_" . $this->getTable();
        parent::__construct($attributes);
    }
}
