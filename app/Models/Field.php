<?php

namespace App\Models;


class Field
{
    public $name;
    public $rules;
    public $type;

    public function __construct($name, $rules = [], $type = "String")
    {
        $this->name = $name;
        $this->rules = $rules;
        $this->type = $type;
    }
}
