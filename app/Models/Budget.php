<?php

namespace App\Models;

use App\Area;
use App\Models\Budget\Activity;
use App\Models\StrategicIssues\Objective;
use App\Models\StrategicIssues\Strategies;
use App\Models\Model;

class Budget extends Model
{
    public function objective(){
        return $this->belongsTo(Objective::class, "objective_id", "id");
    }
    public function strategy(){
        return $this->belongsTo(Strategies::class, "strategy_id", "id");
    }
    public function activities(){
        return $this->hasMany(Activity::class, "budget_id", "id");
    }
    public function area(){
        return $this->belongsTo(Area::class, "area_id", "id");
    }
}
