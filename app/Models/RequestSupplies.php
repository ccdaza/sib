<?php

namespace App\Models;

use App\CategoriesShopping;
use App\Models\Budget\Resource;

class RequestSupplies extends Model
{
    protected $table = "request_supplies";

    public function resource(){
        return $this->belongsTo(Resource::class, "resources_id", "id");
    }
    public function category(){
        return $this->belongsTo(CategoriesShopping::class, "category_id", "id");
    }
}
