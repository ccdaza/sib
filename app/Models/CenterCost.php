<?php

namespace App\Models;

use App\Models\Model;

class CenterCost extends Model
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = "center_costs";
    }
}
