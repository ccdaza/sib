<?php

namespace App\Models\Dofa;

use App\Models\StrategicIssues\StrategyIssues;
use App\Models\Model;

class WeaknessOpportunity extends Model
{
    public function weakness(){
        return $this->belongsTo(Weaknesses::class, "weakness_id", "id");
    }
    public function opportunity(){
        return $this->belongsTo(Opportunity::class, "opportunity_id", "id");
    }
    public function strategySummary(){
        return $this->belongsTo(StrategyIssues::class, "id", "model_id")->where("model", "\\" . self::class);
    }
}
