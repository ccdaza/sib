<?php

namespace App\Models\Dofa;

use App\User;
use App\Models\Model;

class UsersQuality extends Model
{
    public function user(){
        return $this->belongsTo(User::class, "user_id", "id");
    }
}
