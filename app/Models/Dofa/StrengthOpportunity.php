<?php

namespace App\Models\Dofa;

use App\Models\StrategicIssues\StrategyIssues;
use App\Models\Model;

class StrengthOpportunity extends Model
{
    public function strength(){
        return $this->belongsTo(Strength::class, "strength_id", "id");
    }
    public function opportunity(){
        return $this->belongsTo(Opportunity::class, "opportunity_id", "id");
    }

    public function strategySummary(){
        return $this->belongsTo(StrategyIssues::class, "id", "model_id")->where("model", "\\" . self::class);
    }
}
