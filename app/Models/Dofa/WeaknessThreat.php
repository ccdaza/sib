<?php

namespace App\Models\Dofa;

use App\Models\StrategicIssues\StrategyIssues;
use App\Models\Model;

class WeaknessThreat extends Model
{
    public function weakness(){
        return $this->belongsTo(Weaknesses::class, "weakness_id", "id");
    }
    public function threat(){
        return $this->belongsTo(Threats::class, "threat_id", "id");
    }
    public function strategySummary(){
        return $this->belongsTo(StrategyIssues::class, "id", "model_id")->where("model", "\\" . self::class);
    }
}
