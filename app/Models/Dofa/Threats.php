<?php

namespace App\Models\Dofa;

use App\Models\Model;
use App\Utils;

class Threats extends Model
{
    protected $table = "dofa_threats";
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    static function higher(){
        $prefix = Utils::prefix();
        return Threats::select($prefix . "dofa_threats.*")
                        ->join($prefix . "pestel_aspects", $prefix . "pestel_aspects.id", "=", $prefix . "dofa_threats.aspect_id")
                        ->where($prefix . "pestel_aspects.intensity", 3)->get();
    }
}
