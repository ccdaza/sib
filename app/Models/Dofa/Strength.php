<?php

namespace App\Models\Dofa;

use App\Models\Model;

class Strength extends Model
{
    protected $table = "dofa_strengths";
}
