<?php

namespace App\Models\Dofa;

use App\Models\Model;
use App\Utils;

class Opportunity extends Model
{
    public $table = "dofa_opportunities";
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    static function higher(){
        $prefix = Utils::prefix();
        return Opportunity::select($prefix . "dofa_opportunities.*")
                            ->join($prefix . "pestel_aspects", $prefix . "pestel_aspects.id", "=", $prefix . "dofa_opportunities.aspect_id")
                            ->where($prefix . "pestel_aspects.intensity", 3)->get();
    }
}
