<?php

namespace App\Models;

use App\Area;
use App\Models\Model;

class Office extends Model
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = "offices";
    }

    public function area(){
        return $this->belongsTo(Area::class, "area_id", "id");
    }
}
