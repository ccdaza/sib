<?php

namespace App\Models;

use App\Area;
use App\Models\Model;

class ActivitiesExtraActivity extends Model
{
    public $table = "activities_extra_activities";
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function resources(){
        return $this->hasMany(ActivitiesExtraResource::class, "activity_id", "id");
    }

}
