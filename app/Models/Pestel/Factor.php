<?php

namespace App\Models\Pestel;

use App\Models\Model;

class Factor extends Model
{

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = "pestel_factors";
    }
}
