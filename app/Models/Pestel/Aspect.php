<?php

namespace App\Models\Pestel;

use App\Models\Dofa\Opportunity;
use App\Models\Dofa\Threats;
use App\Models\Model;

class Aspect extends Model
{
    public $table = "pestel_aspects";
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function threat(){
        return $this->belongsTo(Threats::class, "id", "aspect_id");
    }

    public function opportunity(){
        return $this->belongsTo(Opportunity::class, "id", "aspect_id");
    }

    public function deleteDofa(){
        if(!is_null($this->opportunity)){
            $this->opportunity->delete();
        }
        if(!is_null($this->threat)){
            $this->threat->delete();
        }
    }

    public function dofa(){
        $opportunity = $this->selectRaw("'opportunity' as dofa")
                            ->join("dofa_opportunities", "dofa_opportunities.aspect_id", "=", $this->table . ".id");
        return $this->selectRaw("'threat' as dofa")
                    ->join("dofa_threats", "dofa_threats.aspect_id", "=", $this->table . ".id")
                    ->union($opportunity)
                    ->get();
    }
}
