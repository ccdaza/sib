<?php

namespace App\Models;

use App\User;
use App\Models\Model;

class Notification extends Model
{

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = "notifications";
    }

    static function newNotification(string $text, User $user, string $link){
        $response = true;

        $notification = new self();
        $notification->text = $text;
        $notification->user_id = $user->id;
        $notification->link = $link;
        try{
            $notification->save();
        }catch (\Exception $e){
            $response = false;
        }
        return $response;
    }
}
