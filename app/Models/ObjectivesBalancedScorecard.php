<?php

namespace App\Models;

use App\Models\Model;

class ObjectivesBalancedScorecard extends Model
{
    public function balancedScorecard(){
        return $this->belongsTo(BalancedScorecard::class, "balanced_scorecard_id", "id");
    }
}
