<?php

namespace App\Models;

use App\Area;
use App\User;
use App\Models\Model;

class BalancedScorecard extends Model
{
    public function responsible_measurement(){
        return $this->belongsTo(User::class, "responsible_measurement_id", "id");
    }
    public function head_analysis(){
        return $this->belongsTo(User::class, "head_analysis_id", "id");
    }
    public function area(){
        return $this->belongsTo(Area::class, "area_id", "id");
    }
    public function objectiveBalancedScorecard(){
        return $this->belongsTo(ObjectivesBalancedScorecard::class, "id", "balanced_scorecard_id");
    }
}
