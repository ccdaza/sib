<?php

namespace App\Models\InterestGroups;

use App\Models\Model;

class Need extends Model
{
    protected $table = "interest_groups_needs";
}
