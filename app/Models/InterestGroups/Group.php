<?php

namespace App\Models\InterestGroups;

use App\StrategicIssuesMatrixScore;
use App\Models\Model;
use Illuminate\Support\Facades\Auth;

class Group extends Model
{
    protected $table = "interest_groups";


    public function userScore(){
        return $this->hasMany(StrategicIssuesMatrixScore::class, "data_id", "id")
            ->where("type", "interestGroups")
            ->where("user_id", Auth::id());
    }
}
