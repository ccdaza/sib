<?php

namespace App\Models\InterestGroups;

use App\Models\Model;

class Expectation extends Model
{
    protected $table = "interest_groups_expectations";
}
