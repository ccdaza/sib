<?php

namespace App\Models\InterestGroups;

use App\Models\Model;

class Category extends Model
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = "interest_groups_categories";
    }

}
