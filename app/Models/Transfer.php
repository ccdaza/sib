<?php

namespace App\Models;

use App\Area;
use App\Models\Model;
use App\User;

class Transfer extends Model
{
    public $table = "transfers";
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function Area(){
        return $this->belongsTo(Area::class, "area_id", "id");
    }

    public function AreaSaldo(){
        return $this->belongsTo(Area::class, "area_id", "id");
    }

    public function ejecutor(){
        return $this->belongsTo(User::class, "ejecutor_id", "id");
    }

}
