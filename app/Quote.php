<?php

namespace App;

use App\Models\Model;
use App\Models\RequestSupplies;

class Quote extends Model
{
    public function supply(){
        return $this->belongsTo(RequestSupplies::class, "request_supply_id", "id");
    }
    public function provider1(){
        return $this->belongsTo(Provider::class, "provider1_id", "id");
    }
    public function provider2(){
        return $this->belongsTo(Provider::class, "provider2_id", "id");
    }
    public function provider3(){
        return $this->belongsTo(Provider::class, "provider3_id", "id");
    }
    public function getProvider1RatingsAttribute($value){
        return json_decode($value);
    }
    public function getProvider2RatingsAttribute($value){
        return json_decode($value);
    }
    public function getProvider3RatingsAttribute($value){
        return json_decode($value);
    }
}
