<?php

namespace App;

use App\Models\Budget\Activity;
use App\Models\Model;

class RequestMoney extends Model
{
    public function activity(){
        return $this->belongsTo(Activity::class, "activity_id", "id");
    }
}
