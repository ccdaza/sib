<?php

namespace App;

use App\Models\Model;

class StrategicIssuesMatrixUsers extends Model
{
    public function user(){
        return $this->belongsTo(User::class, "user_id", "id");
    }
}
