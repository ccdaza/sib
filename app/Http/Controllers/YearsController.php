<?php

namespace App\Http\Controllers;

use App\Models\Field;
use App\Year;
use database\migrations\migrations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class YearsController extends Controller
{
    public function __construct()
    {
        $this->name = "Años";
        $this->setField(new Field("year", ["required"]));
        $this->setModel(Year::class);
    }

    public function index(Request $request){
        $years = Year::orderBy("year", "asc")->get();
        return $this->indexController($request, compact("years"));
    }

    public function store(Request $request)
    {
        try{
            migrations::newDB($request->year);
        }catch (\Exception $e){
            return response()->json(["message" => $e->getMessage()], 400);
        }

        return parent::store($request);
    }
}
