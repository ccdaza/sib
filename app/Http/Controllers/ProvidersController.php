<?php

namespace App\Http\Controllers;

use App\CategoriesShopping;
use App\Models\CenterCost;
use App\Models\RequestSupplies;
use App\Provider;
use App\Quote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProvidersController extends Controller
{

    public function index(){
        $providers = Provider::orderBy("created_at", "DESC")->paginate();
        return view("Providers.index", compact("providers"));
    }

    public function create(){

        return view("Providers.create");

    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            "name" => "required",
            "email" => "required",
            "phone" => "required",
            "adviser" => "required"
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        $provider = new Provider();
        $provider->name = $request->name;
        $provider->email = $request->email;
        $provider->phone = $request->phone;
        $provider->nit = $request->nit;
        $provider->adviser = $request->adviser;
        $provider->direccion = $request->direccion;
        $provider->city = $request->city;
        $provider->state = false;

        try{
            $provider->save();
        }catch (\Exception $e){
            $validate->errors()->add("error", $e->getMessage());
            return redirect()->back()->withErrors($validate)->withInput();
        }

        return redirect()->route("providers.index");

    }

    public function finalizar($provider){
        $provider = Provider::find($provider);
        return view("Providers.finish", compact("provider"));
    }

    public function rut($provider){
        $provider = Provider::find($provider);
        if(is_null($provider->rut)){
            return response("");
        }
        $file = Storage::get($provider->rut);
        $type = Storage::mimeType($provider->rut);
        return response()->make($file)->header('Content-Type', $type);
    }

    public function camara($provider){
        $provider = Provider::find($provider);
        if(is_null($provider->camara)){
            return response("");
        }
        $file = Storage::get($provider->camara);
        $type = Storage::mimeType($provider->camara);
        return response()->make($file)->header('Content-Type', $type);
    }

    public function updateFilesLink($hash){
        $provider = str_replace("proveedor-", "", base64_decode($hash));
        $provider = Provider::find($provider);
        return view("Providers.finishLink", compact("provider"));
    }

    public function updateFiles(Request $request, $provider){
        $validate = Validator::make($request->all(), [
            "rut" => "required|file|mimes:pdf",
            "camara" => "required|file|mimes:pdf"
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $provider = Provider::find($provider);

        try{
            $provider->rut = Storage::putFileAs('proveedores/' . $provider->id, $request->file("rut"), "rut.pdf");
        }catch (\Exception $e){
            $validate->errors()->add("rut", "No fue posible subir el archivo para RUT");
            return redirect()->back()->withErrors($validate)->withInput();
        }

        try{
            $provider->camara = Storage::putFileAs('proveedores/' . $provider->id, $request->file("camara"), "camara.pdf");
        }catch (\Exception $e){
            $validate->errors()->add("camara", "No fue posible subir el archivo para Camara de comercio");
            return redirect()->back()->withErrors($validate)->withInput();
        }

        $provider->state = true;
        try{
            $provider->save();
        }catch (\Exception $e){
            $validate->errors()->add("error", $e->getMessage());
            return redirect()->back()->withErrors($validate)->withInput();
        }

        return redirect()->route("providers.index");

    }

    public function updateFiles2(Request $request, $provider){
        $validate = Validator::make($request->all(), [
            "rut" => "required|file|mimes:pdf",
            "camara" => "required|file|mimes:pdf"
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $provider = Provider::find($provider);

        try{
            $provider->rut = Storage::putFileAs('proveedores/' . $provider->id, $request->file("rut"), "rut.pdf");
        }catch (\Exception $e){
            $validate->errors()->add("rut", "No fue posible subir el archivo para RUT");
            return redirect()->back()->withErrors($validate)->withInput();
        }

        try{
            $provider->camara = Storage::putFileAs('proveedores/' . $provider->id, $request->file("camara"), "camara.pdf");
        }catch (\Exception $e){
            $validate->errors()->add("camara", "No fue posible subir el archivo para Camara de comercio");
            return redirect()->back()->withErrors($validate)->withInput();
        }

        try{
            $provider->save();
        }catch (\Exception $e){
            $validate->errors()->add("error", $e->getMessage());
            return redirect()->back()->withErrors($validate)->withInput();
        }

        return view("providers.thanks", compact("provider"));

    }

    public function show($provider){
        $provider = Provider::find($provider);
        return view("Providers.show", compact("provider"));

    }

    public function update(Request $request, $provider){
        $validate = Validator::make($request->all(), [
            "name" => "required",
            "email" => "required",
            "phone" => "required",
            "adviser" => "required"
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        $provider = Provider::find($provider);
        $provider->name = $request->name;
        $provider->email = $request->email;
        $provider->phone = $request->phone;
        $provider->nit = $request->nit;
        $provider->adviser = $request->adviser;
        $provider->direccion = $request->direccion;
        $provider->city = $request->city;

        try{
            $provider->save();
        }catch (\Exception $e){
            $validate->errors()->add("error", $e->getMessage());
            return redirect()->back()->withErrors($validate)->withInput();
        }

        return redirect()->route("providers.index");

    }

    public function listJson(Request $request){
        $page = $request->get("page", 1);
        $length = $request->get("length", 10);
        $find = $request->get("find", "");

        $response = Provider::select("id", "name as text");

        if($request->has("find") && $find != ''){
            $response->where("name", "LIKE", "%$find%");
        }

        $response = $response->orderBy("name", "ASC")->paginate($length)->toArray();

        $data = [
            "results" => $response["data"],
            "pagination" => [
                "more" => !is_null($response["next_page_url"])
            ]
        ];

        return response()->json($data);
    }

}
