<?php

namespace App\Http\Controllers\StrategySummary;

use App\Models\Field;
use App\Models\StrategicIssues\Category;
use App\Models\Dofa\StrengthOpportunity;
use App\Models\Dofa\StrengthThreat;
use App\Models\Dofa\WeaknessOpportunity;
use App\Models\Dofa\WeaknessThreat;
use App\Models\StrategicIssues\Strategies;
use App\Models\StrategicIssues\StrategyIssues;
use App\Workflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as MainController;
use Illuminate\Support\Str;

class Controller extends MainController
{
    public function __construct()
    {
        $this->name = "Resumen de estrategias";
        $this->depends = [
            \App\Http\Controllers\Dofa\Controller::class
        ];
        $this->getTask();

        $this->fileLang = "StrategySummary";
        $this->setField(new Field("category_id", ["required"]));
        $this->setField(new Field("strategy_id", ["required"]));
        $this->setField(new Field("model", ["required"]));
        $this->setField(new Field("model_id", ["required"]));
        $this->setModel(StrategyIssues::class);
    }

    public function index(Request $request){

        $categories = Category::orderBy("name", "asc")->get();
        $strategies = Strategies::orderBy("name", "asc")->get();
        $strengthsOpportunities = StrengthOpportunity::where("score", ">=", 3)->with("strength", "opportunity", "strategySummary", "strategySummary")->get();
        $strengthsThreat = StrengthThreat::where("score", ">=", 3)->with("strength", "threat", "strategySummary")->get();
        $weaknessesOpportunities = WeaknessOpportunity::where("score", ">=", 3)->with("weakness", "opportunity", "strategySummary")->get();
        $weaknessesThreat = WeaknessThreat::where("score", ">=", 3)->with("weakness", "threat", "strategySummary")->get();

        return $this->indexController($request, [
            "categories" => $categories,
            "strategies" => $strategies,
            "strengthsOpportunities" => $strengthsOpportunities,
            "strengthsThreat" => $strengthsThreat,
            "weaknessesThreat" => $weaknessesThreat,
            "weaknessesOpportunities" => $weaknessesOpportunities,
        ]);
    }

    public function store(Request $request)
    {
        $strategyId = null;
        if(intval($request->strategy) == 0){
            $strategy = new Strategies();
            $strategy->name = $request->strategy;
            try{
                $strategy->save();
            }catch (\Exception $e){
                return response()->json([
                    "message" => __($this->fileLang . ".create.failed")
                ], 400);
            }
            $strategyId = $strategy->id;
        }else{
            $strategyId = $request->strategy;
        }
        $request->request->add(["strategy_id" => $strategyId]);

        $model = "\App\Models\Dofa\\";
        $parts = explode("-", $request->modelParts);
        $model .= ucfirst($parts[0]) . ucfirst($parts[1]);
        $request->request->add(["model" => $model]);

        $strategySummary = StrategyIssues::where("model", $model)->where("model_id", $request->model_id)->get();

        if($strategySummary->count() > 0){
            return parent::update($request, $strategySummary->last()->id);
        }else{
            return parent::store($request);
        }
    }



    public function finish(Request $request){

        $this->task->state = true;
        try{
            $this->task->save();
        }catch (\Exception $e){
            return response()->json([
                "message" => __($this->fileLang . ".finish.failed")
            ], 400);
        }

        return response()->json([
            "message" => __($this->fileLang . ".finish.success")
        ]);
    }
}
