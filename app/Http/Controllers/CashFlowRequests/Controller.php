<?php

namespace App\Http\Controllers\CashFlowRequests;

use App\Models\Notification;
use App\RequestMoney;
use App\Workflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as MainController;
use Illuminate\Support\Facades\Auth;

class Controller extends MainController
{

    public function __construct()
    {
        $this->fileLang = "CashFlowRequest";
        $this->name = "Solicitud de flujo de caja";
        $this->depends = [
            \App\Http\Controllers\Budget\ApproveController::class,
        ];
        $this->getTask();
    }

    public function index(Request $request){
        $requests = RequestMoney::orderBy("created_at", "asc")->where("state", 0)->with("activity.resources.centerCost", "activity.office", "activity.responsable", "activity.executor", "activity.budget.area")->get();
        return $this->indexController($request, compact("requests"));
    }

    public function approve($request){
        $request = RequestMoney::find($request);
        $request->state = 1;
        try{
            $request->save();
        }catch (\Exception $e){
            return response()->json([
                "message" => __($this->fileLang . ".approve.failed")
            ], 400);
        }

        return response()->json([
            "message" => __($this->fileLang . ".approve.success")
        ]);
    }

    public function refuse($request){
        $request = RequestMoney::find($request);
        $request->state = 2;
        try{
            $request->save();
        }catch (\Exception $e){
            return response()->json([
                "message" => __($this->fileLang . ".refuse.failed")
            ], 400);
        }

        return response()->json([
            "message" => __($this->fileLang . ".refuse.success")
        ]);
    }
}
