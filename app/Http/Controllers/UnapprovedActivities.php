<?php

namespace App\Http\Controllers;

use App\RequestMoney;
use Illuminate\Http\Request;

class UnapprovedActivities extends Controller
{
    public function index(Request $request)
    {
        $activities = RequestMoney::where([
            "state" => 2
        ])->get();
        return view("UnapprovedActivities", compact("activities"));
    }
}
