<?php

namespace App\Http\Controllers\StrategicIssues;

use App\Models\StrategicIssues\Category;
use App\Models\StrategicIssues\Objective;
use App\Models\StrategicIssues\Perspective;
use App\Models\StrategicIssues\RelationObjective;
use App\Workflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as MainController;

class Controller extends MainController
{

    public function __construct()
    {
        $this->fileLang = "StrategicIssues";
        $this->name = "Temas estrategicos";
        $this->depends = [
            \App\Http\Controllers\StrategySummary\Controller::class,
            \App\Http\Controllers\StrategicIssuesMatrix\Controller::class,
        ];
        $this->getTask();
    }

    public function index(Request $request){
        $categories = Category::orderBy("name", "asc")->with("strategicIssues.strategy", "objectives")->get();
        $perspectives = Perspective::orderBy("name", "asc")->get();
        return $this->indexController($request, compact("categories", "perspectives"));
    }

    public function saveObjective(Request $request){
        if($request->has("objective_id")){
            $objective = Objective::find($request->objective_id);
        }else{
            $objective = new Objective();
            $objective->category_id = $request->category;
        }
        $objective->objective = $request->objective;
        $objective->perspective_id = $request->perspective;
        $objective->premise = $request->premise;
        try{
            $objective->save();
        }catch (\Exception $e){
            return response()->json([
                "message" => $this->fileLang . ".objectives.created.failed" . " (" . $e->getMessage() . ")"
            ], 400);
        }

        return response()->json(["message" => $this->fileLang . ".objectives.created.success"]);
    }


    public function finish(Request $request){
        $this->task->state = true;
        try{
            $this->task->save();
        }catch (\Exception $e){
            return response()->json([
                "message" => __($this->fileLang . ".finish.failed")
            ], 400);
        }

        return response()->json([
            "message" => __($this->fileLang . ".finish.success")
        ]);
    }
}
