<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function getNotifications(){
        $notifications = Notification::where("user_id", Auth::id())
                                    ->where("read", false)
                                    ->orderBy("created_at", "desc")->get();
        return response()->json([
            "total" => $notifications->count() > 9 ? "+9" : $notifications->count(),
            "notifications" => $notifications->take(5)
        ]);
    }
}
