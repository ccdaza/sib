<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Passport\Bridge\AccessToken;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, $request->remember)) {

            Auth::user()->deleteTokens();

            return response()->json($this->responseSuccess());
        }else{
            return response()->json([
                "message" => __("auth.failed")
            ], 401);
        }
    }

    public function intranetLogin(Request $request){

        $users = \App\User::where("email", $request->email)->get();
        if($users->count() > 0){
            Auth::login($users->last(), true);
            $token = Auth::user()->createToken('Token_' . Auth::user()->name)->accessToken;
            return response()->json([
                "error" => false,
                "token" => $token
            ]);
        }else{
            return response()->json([
                "error" => true,
                "mensaje" => "No fue posible logear al usuario"
            ]);
        }

    }

    public function setYear(Request $request){
        Session::put("year", $request->get("year", date("Y")));
    }

    public function loginWithGoogle(){
        return Socialite::driver('google')->redirect();
    }

    public function googleCallback(){
        try{
            $userGoogle = Socialite::driver("google")->user();
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            abort(403, 'Unauthorized action.');
            return redirect()->to('/');
        }

        $users = \App\User::where("email", $userGoogle->email)->get();

        if($users->count() > 0){
            Auth::login($users->last(), true);
            return redirect("/responseGoogle")->with("response", $this->responseSuccess());
        }else{
            return redirect("/responseGoogle")->with("response", [
                "state" => "failed",
                "message" => __("auth.failed")
            ]);
        }
    }

    private function responseSuccess(){

        $token = Auth::user()->createToken('Token_' . Auth::user()->name)->accessToken;

        return [
            "state" => "success",
            "message" => __("auth.success"),
            "user" => User::where("id", Auth::id())->with("office.area")->get()->last(),
            "token" => $token,
            "year" => session("year")
        ];
    }

    public function responseIntranet(Request $request){
        return view("authIntranet");
    }
}
