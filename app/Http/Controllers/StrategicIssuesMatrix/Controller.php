<?php

namespace App\Http\Controllers\StrategicIssuesMatrix;

use App\Mail\StrategicIssuesMatrixRequest;
use App\Models\InterestGroups\Group;
use App\Models\Notification;
use App\Models\StrategicIssues\Category;
use App\Models\StrategicIssues\StrategyIssues;
use App\StrategicIssuesMatrixScore;
use App\StrategicIssuesMatrixUsers;
use App\User;
use App\Workflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as MainController;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class Controller extends MainController
{
    public function __construct()
    {
        $this->fileLang = "StrategicIssuesMatrix";
        $this->name = "Matriz de cobertura de los temas estrategicos";
        $this->depends = [
            \App\Http\Controllers\Dofa\Controller::class,
            \App\Http\Controllers\InterestGroups\Controller::class
        ];
        $this->getTask();
    }

    public function index(Request $request){
        $strategicIssues = StrategyIssues::orderBy("id", "asc")->with("strategy")->get();
        $strategicIssues->map(function($item, $key) use ($strategicIssues) {
            $m = $item->toArray();
            $s = [
                    "score" => StrategicIssuesMatrixScore::selectRaw("strategic_issues_category_id as category_id, count(*) as count, sum(score) as sum")
                        ->where("type", "strategicIssues")
                        ->where("data_id", $item->id)
                        ->groupBy("strategic_issues_category_id")->get()
                ];
            $strategicIssues->put($key, array_merge($m, $s));
        });
        $interestGroups = Group::orderBy("name", "asc")->get();
        $interestGroups->map(function($item, $key) use ($interestGroups) {
            $m = $item->toArray();
            $s = [
                "score" => StrategicIssuesMatrixScore::selectRaw("strategic_issues_category_id as category_id, count(*) as count, sum(score) as sum")
                    ->where("type", "interestGroups")
                    ->where("data_id", $item->id)
                    ->groupBy("strategic_issues_category_id")->get()
            ];
            $interestGroups->put($key, array_merge($m, $s));
        });
        $strategicIssuesCategories = Category::orderBy("name", "asc")->get();
        $strategies = collect([
            [
                "id" => 1,
                "name" => "Misión",
                "score" => StrategicIssuesMatrixScore::selectRaw("strategic_issues_category_id as category_id, count(*) as count, sum(score) as sum")
                    ->where("type", "strategies")
                    ->where("data_id", 1)
                    ->groupBy("strategic_issues_category_id")->get()
            ],
            [
                "id" => 2,
                "name" => "Visión",
                "score" => StrategicIssuesMatrixScore::selectRaw("strategic_issues_category_id as category_id, count(*) as count, sum(score) as sum")
                    ->where("type", "strategies")
                    ->where("data_id", 2)
                    ->groupBy("strategic_issues_category_id")->get()
            ],
            [
                "id" => 3,
                "name" => "Politica de calidad",
                "score" => StrategicIssuesMatrixScore::selectRaw("strategic_issues_category_id as category_id, count(*) as count, sum(score) as sum")
                    ->where("type", "strategies")
                    ->where("data_id", 3)
                    ->groupBy("strategic_issues_category_id")->get()
            ]
        ]);

        $usersCanReview = $this->usersCanReview()->toArray();
        $usersCanScore = $this->usersCanScore()->toArray();

        $userCanScore = Arr::where($usersCanScore, function ($value, $key) use ($request){
            return $value["user_id"] == $request->user()->id;
        });

        $canSee = count($userCanScore) > 0 ? true : $request->user()->canSee($this);
        $onlyScore = count($userCanScore) > 0 && !$request->user()->canSee($this);

        return $this->indexController($request, compact("strategicIssues", "strategicIssuesCategories", "interestGroups", "strategies", "usersCanReview", "usersCanScore", "canSee", "onlyScore"));
    }

    public function indexScore(Request $request){
        $strategicIssues = StrategyIssues::orderBy("id", "asc")->with("strategy", "userScore")->get();
        $interestGroups = Group::orderBy("name", "asc")->with("userScore")->get();
        $strategicIssuesCategories = Category::orderBy("name", "asc")->get();
        $strategies = collect([
            [
                "id" => 1,
                "name" => "Misión",
                "user_score" => StrategicIssuesMatrixScore::where("type", "strategies")
                                                            ->where("data_id", 1)
                                                            ->where("user_id", Auth::id())->get()
            ],
            [
                "id" => 2,
                "name" => "Visión",
                "user_score" => StrategicIssuesMatrixScore::where("type", "strategies")
                    ->where("data_id", 2)
                    ->where("user_id", Auth::id())->get()
            ],
            [
                "id" => 3,
                "name" => "Politica de calidad",
                "user_score" => StrategicIssuesMatrixScore::where("type", "strategies")
                    ->where("data_id", 3)
                    ->where("user_id", Auth::id())->get()
            ]
        ]);
        $usersCanScore = $this->usersCanScore()->toArray();

        $userCanScore = Arr::where($usersCanScore, function ($value, $key) use ($request){
            return $value["user_id"] == $request->user()->id;
        });

        $canSee = count($userCanScore) > 0 ? true : $request->user()->canSee($this);
        return $this->indexController($request, compact("strategicIssues", "strategicIssuesCategories", "interestGroups", "strategies", "canSee"));
    }

    public function score(Request $request){
        foreach ($request->scores as $type => $item){
            foreach ($item as $category => $arrayScores){
                foreach ($arrayScores as $dataId => $score){
                    $matrixAll = StrategicIssuesMatrixScore::where("strategic_issues_category_id", $category)
                                                            ->where("type", $type)
                                                            ->where("data_id", $dataId)
                                                            ->where("user_id", Auth::id())->get();
                    if($matrixAll->count() == 0){
                        $matrix = new StrategicIssuesMatrixScore();
                        $matrix->strategic_issues_category_id = $category;
                        $matrix->type = $type;
                        $matrix->data_id = $dataId;
                        $matrix->user_id = Auth::id();
                    }else{
                        $matrix = $matrixAll->last();
                    }
                    $matrix->score = $score;
                    $matrix->save();
                }
            }
        }

        return response()->json([
            "message" => "Calificación enviada con exito"
        ]);
    }

    public function finish(Request $request){
        if($this->triggerWeighing($request->weights)){
            $this->task->state = true;
            try{
                $this->task->save();
            }catch (\Exception $e){
                return response()->json([
                    "message" => __("Dofa.finish.failed")
                ], 400);
            }

            return response()->json([
                "message" => __("Dofa.finish.success")
            ]);

        }else{
            return response()->json([
                "message" => __("Dofa.finish.failed")
            ], 400);
        }
    }

    private function triggerWeighing($weights){
        $categories = Category::all();
        foreach ($categories as $category) {
            $category->weighing = $weights[$category->id];
            $category->save();
        }
        return true;
    }

    public function usersRequest(Request $request){

        foreach ($request->users as $user){
            $userQualify = StrategicIssuesMatrixUsers::where("user_id", $user)->get();
            if($userQualify->count() == 0){
                $userQualify = new StrategicIssuesMatrixUsers();
                $userQualify->user_id = $user;
                $userQualify->save();
            }else{
                $userQualify = $userQualify->last();
            }

            Mail::to($userQualify->user->email)->send(new StrategicIssuesMatrixRequest());
            Notification::newNotification("Usted ha sido seleccionado para calificar los factores MATRIZ DE COBERTURA DE LOS TEMAS ESTRATEGICOS", $userQualify->user, "/");
        }

        return response()->json([
            "message" => __("Dofa.usersQualify.created.success")
        ]);
    }

    public function usersCanReview(){
        $users = User::orderBy("name", "asc")->get();
        return $users;
    }

    public function usersCanScore(){
        $users = StrategicIssuesMatrixUsers::all();
        return $users;
    }
}
