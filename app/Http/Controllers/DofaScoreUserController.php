<?php

namespace App\Http\Controllers;

use App\Models\Dofa\StrengthOpportunity;
use App\Models\Dofa\StrengthsOpportunitiesScoreUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class DofaScoreUserController extends Controller
{

    public function store(Request $request)
    {
        $modelStr = "\App\Models\Dofa\\" . ucfirst($request->key1) . ucfirst($request->key2);
        $modelStrScore = "\App\Models\Dofa\\" . ucfirst(Str::plural($request->key1)) . ucfirst(Str::plural($request->key2)) . "ScoreUser";
        $errors = 0;
        foreach ($request->scores as $id1 => $score){
            foreach ($score as $id2 => $value){
                $model = $modelStr::where($request->key1 . "_id", $id1)->where($request->key2 . "_id", $id2)->get();
                if($model->count() == 0){
                    $errors += 1;
                }else{
                    $modelScore = new $modelStrScore();
                    $modelScore->user_qualify_id = Auth::id();
                    $modelScore->score = $value;
                    $col = $request->key1 . "_" . $request->key2 . "_id";
                    $modelScore->$col = $model->last()->id;
                    try{
                        $modelScore->save();
                    }catch (\Exception $e){
                        $errors += 1;
                    }
                }
            }
        }

        $qualifyCol = ucfirst($request->key1) . ucfirst($request->key2);
        Auth::user()->userQualify->$qualifyCol = true;
        Auth::user()->userQualify->save();

        if($errors > 0){
            return response()->json(["message" => "Ocurrieron algunos errores intentando guardar", 406]);
        }else{
            return response()->json(["message" => "Calificaciones enviadas con exito"]);
        }
    }
}
