<?php

namespace App\Http\Controllers;

use App\Models\CenterCost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CenterCostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $centers = CenterCost::orderBy("name", "asc");
        if(!is_null(Input::get("query"))){
            $centers->orWhere("name", "like", Input::get("query") . "%");
            $centers->orWhere("id", "like", Input::get("query") . "%");
        }
        $centers = $centers->get();
        return response()->json($centers);
    }
}
