<?php

namespace App\Http\Controllers\BalancedScorecard;

use App\Models\BalancedScorecard;
use App\Models\Budget;
use App\Models\Field;
use App\Models\Frequency;
use App\Models\ObjectivesBalancedScorecard;
use App\Models\StrategicIssues\Objective;
use App\Models\StrategicIssues\Perspective;
use App\Workflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as MainController;

class Controller extends MainController
{
    public function __construct()
    {
        $this->fileLang = "BalancedScorecard";
        $this->name = "Cuadro de mando integral";
        $this->depends = [
            \App\Http\Controllers\StrategySummary\Controller::class,
            \App\Http\Controllers\StrategicIssuesMatrix\Controller::class,
            \App\Http\Controllers\StrategicIssues\Controller::class,
        ];
        $this->setModel(BalancedScorecard::class);
        $this->getTask();
        $this->setField(new Field("name", ["required"]));
        $this->setField(new Field("formula", ["required"]));
        $this->setField(new Field("desired_goal", ["required"]));
        $this->setField(new Field("acceptable_goal", ["required"]));
        $this->setField(new Field("minimum_goal", ["required"]));
        $this->setField(new Field("measurement_frequency_id", ["required"]));
        $this->setField(new Field("evaluation_frequency_id", ["required"]));
        $this->setField(new Field("measurement_source", ["required"]));
        $this->setField(new Field("responsible_measurement_id", ["required"]));
        $this->setField(new Field("head_analysis_id", ["required"]));
        $this->setField(new Field("area_id", ["required"]));
    }

    public function index(Request $request){

        $perspectives = Perspective::orderBy("name", "asc")->with("objectives.category.strategicIssues.strategy")->get();
        $perspectives = $perspectives->toArray();
        $perspectives = array_map(function($item){
            $item["objectives"] = array_map(function ($objective){
                $objective["category"]["strategic_issues"] = array_map(function ($strategy) use ($objective){
                    $strategy["balanced_scorecards"] = ObjectivesBalancedScorecard::where("objective_id", $objective["id"])->where("strategy_id", $strategy["strategy_id"])->with("balancedScorecard.responsible_measurement", "balancedScorecard.head_analysis", "balancedScorecard.area")->get();
                    return $strategy;
                }, $objective["category"]["strategic_issues"]);
                return $objective;
            }, $item["objectives"]);
            return $item;
        }, $perspectives);

        $order = [
            "Beneficiarios",
            "Procesos Internos",
            "Aprendizaje y crecimiento",
            "Financiera"
        ];
        $orderPerspectives = [];

        foreach($perspectives as $perspective){
            $orderPerspectives[array_search($perspective["name"], $order)] = $perspective;
        }

        //$perspectives = $orderPerspectives;

        $frequencies = Frequency::all();
        return $this->indexController($request, compact("perspectives", "frequencies"));
    }

    public function store(Request $request)
    {
        $response = parent::store($request);
        if($response->status() == 200){
            $perspectiveBalanced = new ObjectivesBalancedScorecard();
            $perspectiveBalanced->objective_id = $request->objective_id;
            $perspectiveBalanced->strategy_id = $request->strategy_id;
            $perspectiveBalanced->balanced_scorecard_id = $this->getModel()->id;
            try{
                $perspectiveBalanced->save();
            }catch (\Exception $e){
                return response()->json([
                    "message" => __($this->fileLang . ".created.failed" . $e->getMessage())
                ], 400);
            }
            return $response;
        }else{
            return $response;
        }
    }

    public function finish(Request $request){

        if($this->triggerBudget()){
            $this->task->state = true;
            try{
                $this->task->save();
            }catch (\Exception $e){
                return response()->json([
                    "message" => __("Dofa.finish.failed")
                ], 400);
            }

            return response()->json([
                "message" => __("Dofa.finish.success")
            ]);

        }else{
            return response()->json([
                "message" => __("Dofa.finish.failed")
            ], 400);
        }
    }

    /**
     *
     */
    private function triggerBudget(){
        $balancedScorecards = BalancedScorecard::all();
        foreach ($balancedScorecards as $balancedScorecard){
            $budget = new Budget();
            $budget->objective_id = $balancedScorecard->objectiveBalancedScorecard->objective_id;
            $budget->strategy_id = $balancedScorecard->objectiveBalancedScorecard->strategy_id;
            $budget->area_id = $balancedScorecard->area_id;
            $budget->save();
        }
        return true;
    }

    public function update(Request $request, $id)
    {
        return parent::update($request, $id);
    }
}
