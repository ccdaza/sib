<?php

namespace App\Http\Controllers;

use App\Models\Field;
use App\ModuleActionUser;
use App\User;
use App\Workflow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    public function __construct()
    {
        $this->fileLang = "Users";
        $this->name = "Usuarios";
        $this->setModel(User::class);
        $this->setField(new Field("name", ["required"]));
        $this->setField(new Field("email", ["required"]));
        $this->setField(new Field("office_id", ["required"]));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $users = User::orderBy("name", "asc")->with("office.area")->get();
        return $this->indexController($request, compact("users"));
    }

    public function find(Request $request)
    {
        $users = User::orderBy("name", "asc");
        if(!is_null(Input::get("query"))){
            $users->where("name", "like", Input::get("query") . "%");
        }
        if(!is_null(Input::get("office"))){
            $users->where("office_id", Input::get("office"));
        }
        $users = $users->get();
        return response()->json($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $return = parent::store($request);
        $model = $this->getModel();
        $user = User::where("email", $model->email)->get()->last();
        foreach ($request->permissions as $action => $permission){
            if($permission){
                $newPermission = new ModuleActionUser();
                $newPermission->user_id = $user->id;
                $newPermission->action_id = $action;
                try{
                    $newPermission->save();
                }catch (\Exception $e){
                    return response()->json(["message" => "Ocurrio un error, por favor intentelo mas tarde " . $e->getMessage()], 406);
                }
            }
        }

        return $return;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $return = parent::update($request, $id);
        if($request->changePassword){
            $model = $this->getModel();
            $model->password = bcrypt($request->password);
            try{
                $model->save();
            }catch (\Exception $e){
                return response()->json(["message" => "No fue posible actualizar la contraseña"], 406);
            }
        }

        if($request->has("permissions")){
            $permissions = \App\ModuleActionUser::where("user_id", $id)->get();
            $permissionsRequest = $request->permissions;
            foreach($permissions as $permission){
                $permission->delete();
            }

            foreach ($permissionsRequest as $action => $permission){
                if($permission){
                    $newPermission = new ModuleActionUser();
                    $newPermission->user_id = $id;
                    $newPermission->action_id = $action;
                    try{
                        $newPermission->save();
                    }catch (\Exception $e){
                        return response()->json(["message" => "Ocurrio un error, por favor intentelo mas tarde"], 406);
                    }
                }
            }
        }

        return $return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Actualiza el campo avatar del usuario logueado
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function updateAvatar(Request $request){
        $user = $request->user();
        $user->avatar = Storage::putFile("avatars", $request->file("avatar"));
        try{
            $user->save();
        }catch (\Exception $e){
            return response()->json([
                "message" => __("user.failedUpdate")
            ], 400);
        }

        return response()->json([
            "message" => __("user.successUpdate"),
            "user" => $user->only("id", "name", "email", "avatar")
        ]);

    }

    public function avatar($user){
        $user = User::find($user);
        $file = Storage::get($user->avatar);
        $type = Storage::mimeType($user->avatar);
        return response()->make($file)->header('Content-Type', $type);
    }
}
