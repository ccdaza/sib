<?php

namespace App\Http\Controllers;

use App\CategoriesShopping;
use App\Mail\DofaRequest;
use App\Mail\QuoteRequest;
use App\Models\CenterCost;
use App\Models\RequestSupplies;
use App\Provider;
use App\Quote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class QuotesController extends Controller
{

    private $ratings = [
        "valor" => "",
        "carac" => "",
        "cv" => "",
        "precio" => ""
    ];

    public function index(){
        $quotes = Quote::where("state", 0)->orderBy("created_at", "DESC")->with("supply.resource.activity.office.area")->get();
        //dd($quotes->first()->provider1_ratings);
        return view("quotes.index", compact("quotes"));
    }

    public function create(Request $request, $supply){

        $supply = RequestSupplies::find($supply);
        $supply->state = 1;
        $supply->save();

        $categoria = new Quote();
        $categoria->request_supply_id = $supply->id;
        $categoria->save();

        return response()->json(["message" => "Cotización creada enviada con éxito"]);

    }

    public function setProvider(Request $request, $quote, $number){
        $quote = Quote::find($quote);
        $provider_id = "provider" . $number . "_id";
        $quote->$provider_id = $request->category;
        $quote->save();

        return response()->json(["message" => "Proveedor asignado con éxito"]);
    }

    public function setWinner(Request $request, $quote){
        $quote = Quote::find($quote);
        $quote->winner = $request->value;
        $quote->save();

        return response()->json(["message" => "Proveedor asignado con éxito"]);
    }

    public function setValor(Request $request, $quote, $number){

        $provider = "provider" . $number . "_ratings";

        $quote = Quote::find($quote);

        $rating = (object) $this->ratings;

        if($quote->$provider !== null){
            $rating = $quote->$provider;
        }
        if($request->value == ''){
            $quote->winner = null;
        }
        $rating->valor = $request->value;

        $quote->$provider = json_encode($rating);
        $quote->save();

        return response()->json(["message" => "Valor asignado con éxito"]);
    }

    public function setCarac(Request $request, $quote, $number){

        $provider = "provider" . $number . "_ratings";

        $quote = Quote::find($quote);

        $rating = (object) $this->ratings;

        if($quote->$provider !== null){
            $rating = $quote->$provider;
        }

        if($request->value == ''){
            $quote->winner = null;
        }
        $rating->carac = $request->value;

        $quote->$provider = json_encode($rating);
        $quote->save();

        return response()->json(["message" => "Carac asignado con éxito"]);
    }

    public function setCV(Request $request, $quote, $number){

        $provider = "provider" . $number . "_ratings";

        $quote = Quote::find($quote);

        $rating = (object) $this->ratings;

        if($quote->$provider !== null){
            $rating = $quote->$provider;
        }

        if($request->value == ''){
            $quote->winner = null;
        }
        $rating->cv = $request->value;

        $quote->$provider = json_encode($rating);
        $quote->save();

        return response()->json(["message" => "C.V asignado con éxito"]);
    }

    public function setPrecio(Request $request, $quote, $number){

        $provider = "provider" . $number . "_ratings";

        $quote = Quote::find($quote);

        $rating = (object) $this->ratings;

        if($quote->$provider !== null){
            $rating = $quote->$provider;
        }

        if($request->value == ''){
            $quote->winner = null;
        }
        $rating->precio = $request->value;

        $quote->$provider = json_encode($rating);
        $quote->save();

        return response()->json(["message" => "Precio asignado con éxito"]);
    }

    public function finishQuote(Request $request, $quote){
        $quote = Quote::find($quote);

        $provider_id = "provider" . $quote->winner . "_id";
        $provider = Provider::find($quote->$provider_id);
        $quote->state = 1;
        try{
            if(!$provider->state){
                Mail::to($provider->email)->send(new QuoteRequest($provider));
            }
            $quote->save();
        }catch (\Exception $e){
            return redirect()->back()->with("error", "No fue posible finalizar, por favor intentelo mas tarde");
        }

        return redirect()->back()->with("success", "Proceso finalizado con éxito");;
    }

}
