<?php

namespace App\Http\Controllers\Pestel;

use App\Http\Controllers\Dofa\OpportunityController;
use App\Http\Controllers\Dofa\ThreatController;
use App\Models\CompetitivenessMatrix\Factor;
use App\Models\Field;
use App\Models\Pestel\Aspect;
use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AspectController extends Controller
{

    public function __construct()
    {
        $this->fileLang = "Pestel.aspect";
        $this->setField(new Field("factor_id", ["required"]));
        $this->setField(new Field("impact", ["required"]));
        $this->setField(new Field("intensity", ["required"]));
        $this->setModel(Aspect::class);
    }

    public function index(){
        $prefix = Utils::prefix();
        $threats = Aspect::selectRaw($prefix . "pestel_aspects.*, " . $prefix .  "dofa_threats.name as name")->orderBy("factor_id", "asc")
                        ->join($prefix . "dofa_threats", $prefix . "dofa_threats.aspect_id", "=", $prefix . "pestel_aspects.id");
        $aspects = Aspect::selectRaw($prefix . "pestel_aspects.*, " . $prefix . "dofa_opportunities.name as name")->orderBy("factor_id", "asc")
                        ->join($prefix . "dofa_opportunities", $prefix . "dofa_opportunities.aspect_id", "=", $prefix . "pestel_aspects.id")
                        ->union($threats)
                        ->get();
        return response()->json($aspects);
    }

    public function store(Request $request)
    {
        $return = parent::store($request);

        if($return->getStatusCode() == 200){

            $returnDofa = $this->createDofa($request);

            if($returnDofa->getStatusCode() == 200){
                return $return;
            }else{
                return $returnDofa;
            }

        }

        return $return;

    }

    public function update(Request $request, $id)
    {
        $return = parent::update($request, $id);

        if($return->getStatusCode() == 200){

           $this->getModel()->deleteDofa();

            $returnDofa = $this->createDofa($request);

            if($returnDofa->getStatusCode() == 200){
                return $return;
            }

            return $returnDofa;

        }

    }

    public function destroy($id)
    {

        $this->getModel()::find($id)->deleteDofa();

        $return = parent::destroy($id);

        return $return;

    }

    private function createDofa($request){
        $controller = "App\Http\Controllers\Dofa\\" . ucfirst($request->dofa) . "Controller";

        $dofa = new $controller;

        $newRequest = new Request([
            "name" => $request->name,
            "aspect_id" => $this->getModel()->id
        ]);

        return $dofa->store($newRequest);
    }
}
