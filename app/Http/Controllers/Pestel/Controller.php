<?php

namespace App\Http\Controllers\Pestel;

use App\Models\Dofa\Opportunity;
use App\Models\Dofa\Strength;
use App\Models\Dofa\StrengthOpportunity;
use App\Models\Dofa\StrengthThreat;
use App\Models\Dofa\Threats;
use App\Models\Dofa\Weaknesses;
use App\Models\Dofa\WeaknessOpportunity;
use App\Models\Dofa\WeaknessThreat;
use App\Models\Pestel\Factor;
use App\Utils;
use App\Workflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as MainController;
use Illuminate\Support\Facades\Session;

class Controller extends MainController
{

    public function __construct()
    {
        $this->name = "Pestel";
        $this->getTask();
    }

    public function index(Request $request){
        $factors = Factor::orderBy("name", "asc")->get();
        return $this->indexController($request, [
            "factors" => $factors
        ]);
    }

    public function finish(Request $request){
        if($this->triggerTablesDofa()){

            $this->task->state = true;
            try{
                $this->task->save();
            }catch (\Exception $e){
                return response()->json([
                    "message" => __("Pestel.finish.failed")
                ], 400);
            }

            return response()->json([
                "message" => __("Pestel.finish.success")
            ]);

        }else{
            return response()->json([
                "message" => __("Pestel.finish.failed")
            ], 400);
        }

    }

    private function triggerTablesDofa(){
        $prefix = Utils::prefix();
        $strengths = Strength::all();
        $opportunities = Opportunity::higher();
        $threats = Threats::higher();
        $weaknesses = Weaknesses::all();

        foreach ($strengths as $strength){
            foreach ($opportunities as $opportunity){
                $strengthOpportunity = new StrengthOpportunity();
                $strengthOpportunity->strength_id = $strength->id;
                $strengthOpportunity->opportunity_id = $opportunity->id;
                $strengthOpportunity->save();
            }
        }

        foreach ($strengths as $strength){
            foreach ($threats as $threat){
                $strengthThreat = new StrengthThreat();
                $strengthThreat->strength_id = $strength->id;
                $strengthThreat->threat_id = $threat->id;
                $strengthThreat->save();
            }
        }

        foreach ($weaknesses as $weakness){
            foreach ($threats as $threat){
                $WeaknessesThreat = new WeaknessThreat();
                $WeaknessesThreat->weakness_id = $weakness->id;
                $WeaknessesThreat->threat_id = $threat->id;
                $WeaknessesThreat->save();
            }
        }

        foreach ($weaknesses as $weakness){
            foreach ($opportunities as $opportunity){
                $WeaknessesOpportunities = new WeaknessOpportunity();
                $WeaknessesOpportunities->weakness_id = $weakness->id;
                $WeaknessesOpportunities->opportunity_id = $opportunity->id;
                $WeaknessesOpportunities->save();
            }
        }

        return true;

    }
}
