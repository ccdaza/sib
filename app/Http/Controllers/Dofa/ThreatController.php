<?php

namespace App\Http\Controllers\Dofa;

use App\Models\Dofa\Threats;
use App\Models\Field;
use App\Http\Controllers\Controller;

class ThreatController extends Controller
{
    public function __construct()
    {
        $this->fileLang = "Pestel.threats";
        $this->setField(new Field("aspect_id"));
        $this->setField(new Field("name", ["required"]));
        $this->setModel(Threats::class);
    }
}
