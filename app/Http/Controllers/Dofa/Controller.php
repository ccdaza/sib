<?php

namespace App\Http\Controllers\Dofa;

use App\Mail\DofaRequest;
use App\Models\Dofa\Opportunity;
use App\Models\Dofa\Strength;
use App\Models\Dofa\StrengthOpportunity;
use App\Models\Dofa\StrengthsOpportunitiesScoreUser;
use App\Models\Dofa\StrengthsThreatsScoreUser;
use App\Models\Dofa\StrengthThreat;
use App\Models\Dofa\Threats;
use App\Models\Dofa\UsersQuality;
use App\Models\Dofa\Weaknesses;
use App\Models\Dofa\WeaknessesOpportunitiesScoreUser;
use App\Models\Dofa\WeaknessesThreatsScoreUser;
use App\Models\Dofa\WeaknessOpportunity;
use App\Models\Dofa\WeaknessThreat;
use App\Models\Notification;
use App\User;
use App\Utils;
use App\Workflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as MainController;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class Controller extends MainController
{
    public function __construct()
    {
        $this->name = "Dofa";
        $this->depends = [
            \App\Http\Controllers\CompetitivenessMatrix\Controller::class,
            \App\Http\Controllers\Pestel\Controller::class
        ];
        $this->getTask();
    }

    public function index(Request $request){
        $prefix = Utils::prefix();
        $usersCanReview = $this->usersCanReview()->toArray();
        $userCanReview = Arr::where($usersCanReview, function ($value, $key) use ($request){
           return $value["id"] == $request->user()->id;
        });
        $extra = [
            "usersCanReview" => $usersCanReview,
            "strengths" => Strength::all(),
            "opportunities" => Opportunity::higher(),
            "weaknesses" => Weaknesses::all(),
            "threats" => Threats::higher(),
            "canSee" => count($userCanReview) > 0 ? true : $request->user()->canSee($this),
            "onlyScore" => count($userCanReview) > 0 && !$request->user()->canSee($this),
            "strengthsOpportunities" => StrengthsOpportunitiesScoreUser::selectRaw($prefix . "strength_opportunities.*, sum(" . $prefix . "strengths_opportunities_score_users.score)/count(*) as total")
                                                                        ->join($prefix . "strength_opportunities", $prefix . "strength_opportunities.id", "=", $prefix . "strengths_opportunities_score_users.strength_opportunity_id")
                                                                        ->groupBy("strength_opportunity_id")->get(),
            "strengthsThreat" => StrengthsThreatsScoreUser::selectRaw($prefix . "strength_threats.*, sum(" . $prefix . "strengths_threats_score_users.score)/count(*) as total")
                                                            ->join($prefix . "strength_threats", $prefix . "strength_threats.id", "=", $prefix . "strengths_threats_score_users.strength_threat_id")
                                                            ->groupBy("strength_threat_id")->get(),
            "weaknessesOpportunities" => WeaknessesOpportunitiesScoreUser::selectRaw($prefix . "weakness_opportunities.*, sum(" . $prefix . "weaknesses_opportunities_score_users.score)/count(*) as total")
                                                                            ->join($prefix . "weakness_opportunities", $prefix . "weakness_opportunities.id", "=", $prefix . "weaknesses_opportunities_score_users.weakness_opportunity_id")
                                                                            ->groupBy("weakness_opportunity_id")->get(),
            "weaknessesThreat" => WeaknessesThreatsScoreUser::selectRaw($prefix . "weakness_threats.*, sum(" . $prefix . "weaknesses_threats_score_users.score)/count(*) as total")
                                                                ->join($prefix . "weakness_threats", $prefix . "weakness_threats.id", "=", $prefix . "weaknesses_threats_score_users.weakness_threat_id")
                                                                ->groupBy("weakness_threat_id")->get(),
        ];
        return $this->indexController($request, $extra);
    }

    public function finish(Request $request){

        if($this->triggerStrategicIssues()){
            $this->task->state = true;
            try{
                $this->task->save();
            }catch (\Exception $e){
                return response()->json([
                    "message" => __("Dofa.finish.failed")
                ], 400);
            }

            return response()->json([
                "message" => __("Dofa.finish.success")
            ]);

        }else{
            return response()->json([
                "message" => __("Dofa.finish.failed")
            ], 400);
        }
    }

    public function indexScore(Request $request){
        $prefix = Utils::prefix();
        $extra = [
            "canSee" => UsersQuality::where("user_id", Auth::id())->get()->count() > 0,
            "strengths" => Strength::all(),
            "opportunities" => Opportunity::higher(),
            "weaknesses" => Weaknesses::all(),
            "threats" => Threats::higher(),
            "userQualify" => Auth::user()->userQualify,
            "strengthsOpportunities" => StrengthsOpportunitiesScoreUser::selectRaw($prefix . "strengths_opportunities_score_users.*, " . $prefix . "strength_opportunities.strength_id, " . $prefix . "strength_opportunities.opportunity_id")
                                                                        ->join($prefix . "strength_opportunities", $prefix . "strength_opportunities.id", "=", $prefix . "strengths_opportunities_score_users.strength_opportunity_id")
                                                                        ->where("user_qualify_id", Auth::id())->get(),
            "strengthsThreats" => StrengthsThreatsScoreUser::selectRaw($prefix . "strengths_threats_score_users.*, " . $prefix . "strength_threats.strength_id, " . $prefix . "strength_threats.threat_id")
                                                                        ->join($prefix . "strength_threats", $prefix . "strength_threats.id", "=", $prefix . "strengths_threats_score_users.strength_threat_id")
                                                                        ->where("user_qualify_id", Auth::id())->get(),
            "weaknessesOpportunities" => WeaknessesOpportunitiesScoreUser::selectRaw($prefix . "weaknesses_opportunities_score_users.*, " . $prefix . "weakness_opportunities.weakness_id, " . $prefix . "weakness_opportunities.opportunity_id")
                                                                        ->join($prefix . "weakness_opportunities", $prefix . "weakness_opportunities.id", "=", $prefix . "weaknesses_opportunities_score_users.weakness_opportunity_id")
                                                                        ->where("user_qualify_id", Auth::id())->get(),
            "weaknessesThreats" => WeaknessesThreatsScoreUser::selectRaw($prefix . "weaknesses_threats_score_users.*, " . $prefix . "weakness_threats.weakness_id, " . $prefix . "weakness_threats.threat_id")
                                                                        ->join($prefix . "weakness_threats", $prefix . "weakness_threats.id", "=", $prefix . "weaknesses_threats_score_users.weakness_threat_id")
                                                                        ->where("user_qualify_id", Auth::id())->get(),
        ];
        return $this->indexController($request, $extra);
    }

    public function usersCanReview(){
        $users = User::orderBy("name", "asc")->get();
        return $users;
    }

    public function usersRequest(Request $request){

        foreach ($request->users as $user){
            $userQualify = UsersQuality::where("user_id", $user)->get();
            if($userQualify->count() == 0){
                $userQualify = new UsersQuality();
                $userQualify->user_id = $user;
                $userQualify->save();
            }else{
                $userQualify = $userQualify->last();
            }

            Mail::to($userQualify->user->email)->send(new DofaRequest());
            Notification::newNotification("Usted ha sido seleccionado para calificar los factores DOFA", $userQualify->user, "/");
        }

        return response()->json([
            "message" => __("Dofa.usersQualify.created.success")
        ]);
    }

    private function triggerStrategicIssues(){
        $prefix = Utils::prefix();
        $strengthsOpportunities = StrengthOpportunity::selectRaw($prefix . "strength_opportunities.*,  IFNULL(" . $prefix . "strength_opportunities.score, (SELECT sum(" . $prefix . "strengths_opportunities_score_users.score)/count(*) FROM " . $prefix . "strengths_opportunities_score_users WHERE strength_opportunity_id = " . $prefix . "strength_opportunities.id)) as total")->get();
        $strengthsThreat = StrengthThreat::selectRaw($prefix . "strength_threats.*,  IFNULL(" . $prefix . "strength_threats.score, (SELECT sum(" . $prefix . "strengths_threats_score_users.score)/count(*) FROM  " . $prefix . "strengths_threats_score_users WHERE strength_threat_id = " . $prefix . "strength_threats.id)) as total")->get();
        $weaknessesOpportunities = WeaknessOpportunity::selectRaw($prefix . "weakness_opportunities.*,  IFNULL(" . $prefix . "weakness_opportunities.score, (SELECT sum(" . $prefix . "weaknesses_opportunities_score_users.score)/count(*) FROM  " . $prefix . "weaknesses_opportunities_score_users WHERE weakness_opportunity_id = " . $prefix . "weakness_opportunities.id)) as total")->get();
        $weaknessesThreat = WeaknessThreat::selectRaw($prefix . "weakness_threats.*,  IFNULL(" . $prefix . "weakness_threats.score, (SELECT sum(" . $prefix . "weaknesses_threats_score_users.score)/count(*) FROM  " . $prefix . "weaknesses_threats_score_users WHERE weakness_threat_id = " . $prefix . "weakness_threats.id)) as total")->get();

        foreach ($strengthsOpportunities as $item){
            $item->score = round($item->total);
            $item->save();
        }
        foreach ($strengthsThreat as $item){
            $item->score = round($item->total);
            $item->save();
        }
        foreach ($weaknessesOpportunities as $item){
            $item->score = round($item->total);
            $item->save();
        }
        foreach ($weaknessesThreat as $item){
            $item->score = round($item->total);
            $item->save();
        }

        return true;

    }
}
