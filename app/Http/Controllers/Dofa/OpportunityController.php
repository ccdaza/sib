<?php

namespace App\Http\Controllers\Dofa;

use App\Models\Dofa\Opportunity;
use App\Models\Field;
use App\Http\Controllers\Controller;

class OpportunityController extends Controller
{
    public function __construct()
    {
        $this->fileLang = "Pestel.aspect";
        $this->setField(new Field("aspect_id"));
        $this->setField(new Field("name", ["required"]));
        $this->setModel(Opportunity::class);
    }
}
