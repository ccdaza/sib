<?php

namespace App\Http\Controllers;

use App\CategoriesShopping;
use App\Models\CenterCost;
use App\Models\RequestSupplies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CategoriesShoppingController extends Controller
{

    public function index(){
        $categories = CategoriesShopping::orderBy("name", "ASC")->paginate();
        return view("CategoriesShopping.index", compact("categories"));
    }

    public function json(Request $request){
        $page = $request->get("page", 1);
        $length = $request->get("length", 10);
        $find = $request->get("find", "");

        $response = CategoriesShopping::select("id", "name as text");

        if($request->has("find") && $find != ''){
            $response->where("name", "LIKE", "%$find%");
        }

        $response = $response->orderBy("name", "ASC")->paginate($length)->toArray();

        $data = $response;

        return response()->json($data);
    }

    public function create(){
        return view("CategoriesShopping.create");
    }

    public function store(Request $request){

        $categoria = new CategoriesShopping();
        $categoria->name = $request->name;
        $categoria->save();

        return redirect()->route("categoriesShopping.index")->with("success", collect(["Categoria creada con Exito"]));

    }

}
