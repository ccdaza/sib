<?php

namespace App\Http\Controllers;

use App\Models\Field;
use App\Workflow;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $name = "";
    public $depends = [];
    public $fields = [];
    private $model;
    private $rules = [];
    private $messages = [];
    public $fileLang = "";
    public $task;
    public $actions = [
        "Ver"
    ];


    public function setModel($class){
        $this->model = new $class;
        $this->middleware(function($request, $next) use ($class){
            $this->model = new $class;
            return $next($request);
        });
    }

    public function getModel(){
        return $this->model;
    }

    public function getTask(){
       $this->task = Workflow::where("task", $this->name)->get()->last();
    }

    /**
     * @param Field $field
     */
    public function setField(Field $field){
        $this->fields[] = $field;
        foreach ($field->rules as $rule){
            $this->rules[$field->name] = $rule;
            $ruleWithoutAttributes = explode("|", $rule)[0];
            $this->messages[$field->name . "." . $ruleWithoutAttributes] = __($this->fileLang . "." . $field->name . "." . $ruleWithoutAttributes);
        }
    }

    /**
     * @param Request $request
     * @param array $extra
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function indexController(Request $request, $extra = []){
        $canSee = $request->user()->canSee($this);
        $finalized = is_null($this->task) ? false : $this->task->state == 1;
        $response = [
            "canSee" => isset($extra["canSee"]) ? $extra["canSee"] : $canSee,
            "year" => session("year"),
            "finalized" => $finalized,
            "finishedDependencies" => $this->finishedDependencies(),
            "depends" => array_map(function($depend){
                $depend = new $depend();
                return $depend->name;
            }, $this->depends)
        ];
        return response()->json(array_merge($response, $extra));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request){

        $validator = Validator::make($request->all(), $this->rules, $this->messages);

        if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->all()
            ], 406);
        }

        foreach ($this->fields as $field){
            $name = $field->name;
            $this->model->$name = $request->get($name, null);
        }

        try{
            $this->model->save();
        }catch (\Exception $e){
            return response()->json([
                "message" => __($this->fileLang . ".created.failed" . $e->getMessage())
            ], 400);
        }

        return response()->json([
            "message" => __($this->fileLang . ".created.success")
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // obtener item segun id
        $this->model = $this->model::find($id);
        // recorrer campos y asignar valor
        foreach ($this->fields as $field){
            $name = $field->name;
            $this->model->$name = $request->get($name);
        }

        try{
            $this->model->save();
        }catch (\Exception $e){
            return response()->json([
                "message" => __($this->fileLang . ".update.failed")
            ], 400);
        }

        return response()->json([
            "message" => __($this->fileLang . ".update.success")
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->model::find($id);
        try{
            $item->delete();
        }catch (\Exception $e){
            return response()->json([
                "message" => __($this->fileLang . ".deleted.failed")
            ], 400);
        }

        return response()->json([
            "message" => __($this->fileLang . ".deleted.success")
        ]);

    }

    public function finish(Request $request){

        $this->task->state = true;
        try{
            $this->task->save();
        }catch (\Exception $e){
            return response()->json([
                "message" => __($this->fileLang . ".finish.failed")
            ], 400);
        }

        return response()->json([
            "message" => __($this->fileLang . ".finish.success")
        ]);
    }

    private function finishedDependencies(){
        $finished = 0;
        foreach ($this->depends as $depend){
            $depend = new $depend();
            if(is_null($depend->task) || $depend->task->state){
                $finished += 1;
            }
        }
        return $finished == count($this->depends);
    }
}
