<?php

namespace App\Http\Controllers\Budget;

use App\Area;
use App\Models\Budget;
use App\RequestMoney;
use App\Workflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ApprovedController extends Controller
{
    public function __construct()
    {
        $this->fileLang = "Budget";
        $this->name = "Presupuesto aprobado";
        $this->depends = [
            \App\Http\Controllers\Budget\ApproveController::class,
        ];
        $this->getTask();
        $this->setModel(Budget::class);
    }

    public function index(Request $request){
        $budget = Budget::orderBy("objective_id", "asc")->where("area_id", Auth::user()->office->area_id)->where("state", 1)->with("activities.executor","activities.responsable","activities.office","activities.resources.centerCost","activities.request", "objective", "strategy")->get();
        return $this->indexController($request, compact("budget"));
    }

    public function request($activity){
        $requestMoney = RequestMoney::where("activity_id", $activity)->get();
        if($requestMoney->count() == 0){
            $requestMoney = new RequestMoney();
            $requestMoney->activity_id = $activity;
            try{
                $requestMoney->save();
            }catch (\Exception $e){
                return response()->json([
                    "message" => __( $this->fileLang . ".request.failed")
                ], 400);
            }

            return response()->json([
                "message" => __($this->fileLang . ".request.success")
            ]);
        }else{
            return response()->json(["message" => "Este monto ya fue solicitado"]);
        }
    }
}
