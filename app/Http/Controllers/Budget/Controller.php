<?php

namespace App\Http\Controllers\Budget;

use App\Area;
use App\Models\ActivitiesExtra;
use App\Models\ActivitiesExtraActivity;
use App\Models\ActivitiesExtraResource;
use App\Models\Budget;
use App\Models\Transfer;
use App\Workflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as MainController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Controller extends MainController
{
    public function __construct()
    {
        $this->fileLang = "Budget";
        $this->name = "Solicitud de presupuesto";
        $this->depends = [
            \App\Http\Controllers\BalancedScorecard\Controller::class,
        ];
        $this->getTask();
        $this->setModel(Budget::class);
    }

    public function index(Request $request){
        $budget = Budget::orderBy("objective_id", "asc")
            ->where("area_id", Auth::user()->office->area_id)
            ->with("activities.executor","activities.responsable","activities.office","activities.resources.centerCost", "objective", "strategy")
            ->get();
        $canFinish = $request->user()->canFinish($this);
        return $this->indexController($request, compact("budget", "canFinish"));
    }

    public function storeActivity(Request $request, $budget)
    {
        foreach ($request->activities as $activity){
            if(isset($activity["id"])){
                $newActivity = Budget\Activity::find($activity["id"]);
            }else{
                $newActivity = new Budget\Activity();
                $newActivity->budget_id = $budget;
            }
            $newActivity->office_id = $activity["office"]["id"];
            $newActivity->action = $activity["action"];
            $newActivity->description = $activity["description"];
            $newActivity->responsible_id = $activity["responsable"]["id"];
            $newActivity->executor_id = $activity["executor"]["id"];
            $newActivity->start_date = $activity["start_date"];
            $newActivity->end_date = $activity["end_date"];
            try{
                $newActivity->save();
            }catch (\Exception $e){
                return response()->json(["message" => $e->getMessage()], 400);
            }

            foreach ($activity["resources"] as $resource){
                if(isset($resource["id"])){
                    $newResource = Budget\Resource::find($resource["id"]);
                }else{
                    $newResource = new Budget\Resource();
                    $newResource->activity_id = $newActivity->id;
                }
                $newResource->description = $resource["description"];
                $newResource->quantity = $resource["quantity"];
                $newResource->unit = $resource["unit"];
                $newResource->value = $resource["value"];
                try{
                    $newResource->save();
                }catch (\Exception $e){
                    return response()->json(["message" => $e->getMessage()], 400);
                }
            }

        }

        return response()->json(["message" => __($this->fileLang . "activity.create.success")]);
    }

    public function destroyActivity($id)
    {
        $activity = Budget\Activity::find($id);

        foreach ($activity->resources as $resource){
            try{
                $resource->delete();
            }catch (\Exception $e){
                return response()->json(["message" => $e->getMessage()], 400);
            }
        }

        try{
            $activity->delete();
        }catch (\Exception $e){
            return response()->json(["message" => $e->getMessage()], 400);
        }

        return response()->json(["message" => __($this->fileLang . "activity.delete.success")]);
    }

    public function destroyResource($id){

        $resource = Budget\Resource::find($id);

        try{
            $resource->delete();
        }catch (\Exception $e){
            return response()->json(["message" => $e->getMessage()], 400);
        }

        return response()->json(["message" => __($this->fileLang . "resource.delete.success")]);
    }


    public function finish(Request $request){

        $this->task->state = true;
        try{
            $this->task->save();
        }catch (\Exception $e){
            return response()->json([
                "message" => __($this->fileLang . ".finish.failed")
            ], 400);
        }

        return response()->json([
            "message" => __($this->fileLang . ".finish.success")
        ]);
    }


    public function addNewActivity(){
        $areas = Area::all();
        return view("Budget.addNewActivity", compact("areas"));
    }

    public function newActivityExtra(Request $request){
        $extra = new ActivitiesExtra();
        $extra->area_id = $request->uee;
        $extra->ref = $request->ref;

        try{
            $extra->save();
        }catch (\Exception $e){
            return response()->json(["message" => $e->getMessage()], 400);
        }

        foreach ($request->activities as $_activity){
            $activity = new ActivitiesExtraActivity();
            $activity->activities_extra_id = $extra->id;
            $activity->office_id = $_activity["office"]["id"];
            $activity->action = $_activity["action"];
            $activity->description = $_activity["description"];
            $activity->responsable_id = $_activity["responsable"]["id"];
            $activity->executor_id = $_activity["executor"]["id"];
            $activity->start_date = $_activity["start_date"];
            $activity->end_date = $_activity["end_date"];
            $activity->save();

            foreach ($_activity["resources"] as $_resource){
                $resource = new ActivitiesExtraResource();
                $resource->activity_id = $activity->id;
                $resource->description = $_resource["description"];
                $resource->quantity = $_resource["quantity"];
                $resource->unit = $_resource["unit"];
                $resource->value = $_resource["value"];
                $resource->account = $_resource["account"];
                $resource->center_cost_id = 1;
                $resource->save();
            }
        }
        return response()->json(["message" => "Exito"]);

    }

    function transfer(Request $request){
        if($request->method() == "POST"){
            $transfer = new Transfer();
            $transfer->area_id = $request->area;
            $transfer->accion = $request->accion;
            $transfer->ejecutor_id = $request->ejecutor;
            $transfer->valor = $request->valor;
            $transfer->area_saldo_id = $request->areaSaldo;
            $transfer->save();
            $request->session()->flash('success', 'Transferencia creada con éxito');
        }
        $transfers = Transfer::all();
        return view ("Budget/transfer", compact("transfers"));
    }
}
