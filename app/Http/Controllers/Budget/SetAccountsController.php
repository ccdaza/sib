<?php

namespace App\Http\Controllers\Budget;

use App\Models\Budget;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SetAccountsController extends Controller
{
    public function __construct()
    {
        $this->fileLang = "Budget";
        $this->name = "Asignacion de cuentas";
        $this->depends = [
            \App\Http\Controllers\Budget\Controller::class,
        ];
        $this->getTask();
        $this->setModel(Budget::class);
    }

    public function index(Request $request){
        $budget = Budget::orderBy("objective_id", "asc")->with("activities.executor","activities.responsable","activities.office","activities.resources.centerCost", "objective", "strategy", "area")->get();

        return $this->indexController($request, compact("budget"));
    }

    public function update(Request $request, $resource){
        $resource = Budget\Resource::find($resource);
        $resource->account = $request->account;
        $resource->center_cost_id = $request->centerCost;

        try{
            $resource->save();
        }catch (\Exception $e){
            return response()->json(["message" => $e->getMessage()], 400);
        }

        return response()->json(["message" => __($this->fileLang . "setAccount.update.success")]);
    }

}
