<?php

namespace App\Http\Controllers\Budget;

use App\Area;
use App\Models\ActivitiesExtra;
use App\Models\Budget;
use App\User;
use App\Workflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ApproveController extends Controller
{
    public function __construct()
    {
        $this->fileLang = "Budget";
        $this->name = "Aprobacion Presupuesto";
        $this->depends = [
            \App\Http\Controllers\BalancedScorecard\Controller::class,
            \App\Http\Controllers\Budget\Controller::class,
        ];
        $this->getTask();
        $this->setModel(Budget::class);
    }

    public function index(Request $request){
        $areas = Area::orderBy("name", "asc")->with("budgets.objective", "budgets.strategy", "budgets.activities.resources.centerCost", "budgets.activities.office", "budgets.activities.responsable", "budgets.activities.executor")->get();
        $areasExtras = Area::orderBy("name", "asc")->with("extras.activities.resources")->get();
        return $this->indexController($request, compact("areas", "areasExtras"));
    }


    public function finish(Request $request){
        $this->task->state = true;
        try{
            $this->task->save();
        }catch (\Exception $e){
            return response()->json([
                "message" => __($this->fileLang . ".finish.failed")
            ], 400);
        }

        return response()->json([
            "message" => __($this->fileLang . ".finish.success")
        ]);
    }

    public function approve($budget){
        $budget = Budget::find($budget);

        if($budget->state == 0){
            $budget->state = 1;
            try{
                $budget->save();
            }catch (\Exception $e){
                return response()->json(["message" => $e->getMessage()], 400);
            }
        }
        return response()->json(["message" => __($this->fileLang . "approve.success")]);
    }

    public function refuse($budget){
        $budget = Budget::find($budget);

        if($budget->state == 0){
            $budget->state = 2;
            try{
                $budget->save();
            }catch (\Exception $e){
                return response()->json(["message" => $e->getMessage()], 400);
            }
        }

        return response()->json(["message" => __($this->fileLang . "refuse.success")]);
    }

    public function approveExtra($budget){
        $budget = ActivitiesExtra::find($budget);

        if($budget->state == 0){
            $budget->state = 1;
            try{
                $budget->save();
            }catch (\Exception $e){
                return response()->json(["message" => $e->getMessage()], 400);
            }
        }
        return response()->json(["message" => __($this->fileLang . "approve.success")]);
    }

    public function refuseExtra($budget){
        $budget = ActivitiesExtra::find($budget);

        if($budget->state == 0){
            $budget->state = 2;
            try{
                $budget->save();
            }catch (\Exception $e){
                return response()->json(["message" => $e->getMessage()], 400);
            }
        }

        return response()->json(["message" => __($this->fileLang . "refuse.success")]);
    }

    public function approveByArea($area){
        $area = Area::find($area);
        foreach ($area->budgets as $budget){
            if($budget->state == 0){
                $budget->state = 1;
                try{
                    $budget->save();
                }catch (\Exception $e){
                    return response()->json(["message" => $e->getMessage()], 400);
                }
            }
        }

        return response()->json(["message" => __($this->fileLang . "approve.success")]);
    }

    public function refuseByArea($area){
        $area = Area::find($area);
        foreach ($area->budgets as $budget){
            if($budget->state == 0){
                $budget->state = 2;
                try{
                    $budget->save();
                }catch (\Exception $e){
                    return response()->json(["message" => $e->getMessage()], 400);
                }
            }
        }

        return response()->json(["message" => __($this->fileLang . "refuse.success")]);
    }

    public function approveExtraByArea($area){
        $area = Area::find($area);
        foreach ($area->extras as $budget){
            if($budget->state == 0){
                $budget->state = 1;
                try{
                    $budget->save();
                }catch (\Exception $e){
                    return response()->json(["message" => $e->getMessage()], 400);
                }
            }
        }

        return response()->json(["message" => __($this->fileLang . "approve.success")]);
    }

    public function refuseExtraByArea($area){
        $area = Area::find($area);
        foreach ($area->extras as $budget){
            if($budget->state == 0){
                $budget->state = 2;
                try{
                    $budget->save();
                }catch (\Exception $e){
                    return response()->json(["message" => $e->getMessage()], 400);
                }
            }
        }

        return response()->json(["message" => __($this->fileLang . "refuse.success")]);
    }

    public function totalByArea(Request $request){

        $user = User::where("id", Auth::id())->get()->last();

        $budgets = Budget::where("area_id", $user->office->area_id)->where("state", 1)->get();

        $total = 0;

        foreach ($budgets as $budget){
            foreach ($budget->activities as $activity){
                $resources = Budget\Resource::selectRaw("sum(quantity + value) as total")->where("activity_id", $activity->id)->get()->last();
                $total += $resources->total;
            }
        }

        $response = [
            "total" => $total
        ];
        return response()->json($response);
    }

    public function totalByAreaAndMonth(Request $request, $month = null){

        $month = $month ?? intval(date("m"));

        $user = User::where("id", Auth::id())->get()->last();

        $budgets = Budget::where("area_id", $user->office->area_id)->where("state", 1)
                                ->whereHas('activities', function ($query) use ($month) {
                                    $query->where('start_date', $month);
                                })->get();

        $total = 0;
        foreach ($budgets as $budget){
            foreach ($budget->activities as $activity){
                $resources = Budget\Resource::selectRaw("sum(quantity + value) as total")->where("activity_id", $activity->id)->get()->last();
                $total += $resources->total;
            }
        }

        $response = [
            "total" => $total
        ];
        return response()->json($response);
    }

    public function totalByAreaAndMonths(Request $request){

        $user = User::where("id", Auth::id())->get()->last();

        $response = [];

        for($month = 1 ; $month <= 12; $month++){
            $budgets = Budget::where("area_id", $user->office->area_id)->where("state", 1)
                ->whereHas('activities', function ($query) use ($month) {
                    $query->where('start_date', $month);
                })->get();

            $total = 0;
            foreach ($budgets as $budget){
                foreach ($budget->activities as $activity){
                    $resources = Budget\Resource::selectRaw("sum(quantity + value) as total")->where("activity_id", $activity->id)->get()->last();
                    $total += $resources->total;
                }
            }

            $response[] = $total;
        }
        return response()->json($response);
    }
}
