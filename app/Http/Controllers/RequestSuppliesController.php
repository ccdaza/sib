<?php

namespace App\Http\Controllers;

use App\Area;
use App\Models\Budget\Activity;
use App\Models\RequestSupplies;
use App\Models\Budget\Resource;
use App\Models\Field;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class RequestSuppliesController extends Controller
{
    public function __construct()
    {
        $this->fileLang = "Areas";
        $this->name = "Solicitud de insumos";
        $this->setModel(Activity::class);
        $this->setField(new Field("action", ["required"]));
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $activityId)
    {
        $activity = Activity::orderBy("action", "asc")->where("id", $activityId)->with("resources")->get()->first();
        return $this->indexController($request, compact("activity"));
    }

    public function getJson(){
        $areas = Area::orderBy("name", "asc")->get();
        return response()->json($areas);
    }

    public function request(Request $request, $activityId)
    {
        $recursos = Resource::where("activity_id", $activityId)->get();
        foreach ($recursos as $recurso){
            $requestSupples = RequestSupplies::where("resources_id", $recurso->id)->get();
            if($requestSupples->count() == 0){
                $requestSupply = new RequestSupplies();
                $requestSupply->resources_id = $recurso->id;
                $requestSupply->save();
            }
        }
        return response()->json(["message" => "Solicitud enviada con éxito"]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return parent::store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return parent::update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
