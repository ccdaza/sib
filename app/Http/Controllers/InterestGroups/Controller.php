<?php

namespace App\Http\Controllers\InterestGroups;

use App\Area;
use App\Models\Field;
use App\Models\InterestGroups\Category;
use App\Models\InterestGroups\Expectation;
use App\Models\InterestGroups\Gestion;
use App\Models\InterestGroups\Group;
use App\Models\InterestGroups\Need;
use App\Workflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as MainController;
use Illuminate\Support\Facades\Storage;

class Controller extends MainController
{

    public function __construct()
    {
        $this->name = "Grupos de interes";
        $this->fileLang = "interestGroups";
        $this->setModel(Group::class);
        $this->setField(new Field("name", ["required"]));
        $this->setField(new Field("category_id", ["required"]));
        $this->setField(new Field("power", ["required"]));
        $this->setField(new Field("interest", ["required"]));
        $this->setField(new Field("need_id", ["required"]));
        $this->setField(new Field("expectation_id", ["required"]));
        $this->setField(new Field("area_id", ["required"]));
        $this->setField(new Field("risk"));
        $this->setField(new Field("required"));
        $this->getTask();
    }
    public function index(Request $request){

        $categories = Category::orderBy("name", "asc")->get();
        $needs = Need::orderBy("name", "asc")->get();
        $expectations = Expectation::orderBy("name", "asc")->get();
        $areas = Area::orderBy("name", "asc")->get();
        if($request->user()->canSee($this)){
            $groups = Group::all();
        }else{
            $groups = Group::where("area_id", $request->user()->office->area_id)->get();
        }
        $gestions = Gestion::all();
        return $this->indexController($request, [
            "canSee" => true,
            "onlyArea" => !$request->user()->canSee($this),
            "groups" => $groups,
            "categories" => $categories,
            "needs" => $needs,
            "expectations" => $expectations,
            "areas" => $areas,
            "gestions" => $gestions,
        ]);
    }

    public function store(Request $request)
    {
        if(intval($request->need) == 0){
            $need = new Need();
            $need->name = $request->need;
            try{
                $need->save();
            }catch (\Exception $e){
                return response()->json([
                    "message" => __($this->fileLang . ".create.failed")
                ], 400);
            }
            $request->request->add(["need_id" => $need->id]);
        }else {
            $request->request->add(["need_id" => $request->need]);
        }

        if(intval($request->expectation) == 0){
            $expectation = new Expectation();
            $expectation->name = $request->expectation;
            try{
                $expectation->save();
            }catch (\Exception $e){
                return response()->json([
                    "message" => __($this->fileLang . ".create.failed")
                ], 400);
            }
            $request->request->add(["expectation_id" => $expectation->id]);
        }else {
            $request->request->add(["expectation_id" => $request->expectation]);
        }

        return parent::store($request);
    }

    public function finish(Request $request){

        $this->task->state = true;
        try{
            $this->task->saveOrFail();
        }catch (\Exception $e){
            return response()->json([
                "message" => __($this->fileLang . ".finish.failed")
            ], 400);
        }

        return response()->json([
            "message" => __($this->fileLang . ".finish.success")
        ]);
    }

    public function addGestion(Request $request){

        $gestions = Gestion::where("interest_groups_need_id", $request->need_id)->get();
        if($gestions->count() == 0){
            $gestion = new Gestion();
        }else{
            $gestion = $gestions->first();
        }

        $gestion->interest_groups_need_id = $request->need_id;
        $gestion->gestion = $request->gestion;
        if($request->has("file")){
            $gestion->file = Storage::disk("gestions")->putFile("", $request->file("file"));
        }

        try{
            $gestion->saveOrFail();
        }catch (\Exception $e){
            return response()->json([
                "message" => __("No fue posible enviar la gestión"),
                "e" => $e->getMessage()
            ], 400);
        }
        return response()->json([
            "message" => __("Gestión enviada con éxito")
        ]);

    }
}
