<?php

namespace App\Http\Controllers\CompetitivenessMatrix;

use App\Workflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as MainController;

class Controller extends MainController
{

    public function __construct()
    {
        $this->name = "Matriz perfil de competitividad";
        $this->getTask();
    }

    public function index(Request $request){
        return $this->indexController($request);
    }

    public function finish(Request $request){
        $this->task->state = true;
        try{
            $this->task->save();
        }catch (\Exception $e){
            return response()->json([
                "message" => __("CompetitivenessMatrix.finish.failed")
            ], 400);
        }

        return response()->json([
            "message" => __("CompetitivenessMatrix.finish.success")
        ]);

    }
}
