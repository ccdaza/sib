<?php

namespace App\Http\Controllers\CompetitivenessMatrix;

use App\Models\CompetitivenessMatrix\School;
use App\Models\Field;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SchoolController extends Controller
{
    public function __construct()
    {
        $this->fileLang = "CompetitivenessMatrix.school";
        $this->setField(new Field("name", ["required"]));
        $this->setModel(School::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schools = School::orderBy("name", "asc")->with("factorsScore")->get();
        return response()->json($schools);
    }
}
