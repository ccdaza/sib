<?php

namespace App\Http\Controllers\CompetitivenessMatrix;

use App\Models\CompetitivenessMatrix\FactorSchoolScore;
use App\Models\Field;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FactorSchoolScoreController extends Controller
{
    public function __construct()
    {
        $this->fileLang = "CompetitivenessMatrix.FactorSchoolScore";
        $this->setField(new Field("factor_id", ["required"]));
        $this->setField(new Field("school_id", ["required"]));
        $this->setField(new Field("score", ["required"]));
        $this->setModel(FactorSchoolScore::class);
    }
}
