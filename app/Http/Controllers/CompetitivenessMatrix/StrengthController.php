<?php

namespace App\Http\Controllers\CompetitivenessMatrix;

use App\Models\Dofa\Strength;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StrengthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $strength = new Strength();
        $strength->name = $request->name;
        $strength->factor_id = $request->factor_id;

        try{
            $strength->save();
        }catch (\Exception $e){
            return [
                "state" => "failed",
                "message" => __("CompetitivenessMatrix.strength.created.failed")
            ];
        }

        return [
            "state" => "success",
            "message" =>  __("CompetitivenessMatrix.strength.created.success")
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
