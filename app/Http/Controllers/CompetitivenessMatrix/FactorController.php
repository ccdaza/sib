<?php

namespace App\Http\Controllers\CompetitivenessMatrix;

use App\Models\CompetitivenessMatrix\FactorSchoolScore;
use App\Models\Dofa\Strength;
use App\Models\Dofa\Weaknesses;
use App\Models\Field;
use App\Models\StrategicIssues\Strategies;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\CompetitivenessMatrix\Factor;

class FactorController extends Controller
{

    public function __construct()
    {
        $this->fileLang = "CompetitivenessMatrix.factor";
        $this->setField(new Field("name", ["required"]));
        $this->setField(new Field("weighing", ["required"]));
        $this->setModel(Factor::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $factors = Factor::orderBy("name", "asc")->with("strengths", "weaknesses")->get();
        return response()->json($factors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "weighing" => "required"
        ], [
            "name.required" => __("CompetitivenessMatrix.factor.name.required"),
            "weighing.required" => __("CompetitivenessMatrix.factor.weighing.required"),
        ]);

        if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->all()
            ], 406);
        }

        $factor = new Factor();
        $factor->name = $request->name;
        $factor->weighing = $request->weighing;

        try{
            $factor->save();
        }catch (\Exception $e){
            return response()->json([
                "message" => __("CompetitivenessMatrix.factor.created.failed")
            ], 400);
        }

        foreach ($request->factors as $subFactor){
            $controllerName = "App\Http\Controllers\CompetitivenessMatrix\\" . ucfirst($subFactor["dofa"]) . "Controller";
            $controller = new $controllerName();
            $request = new Request([
                "name" => $subFactor["name"],
                "factor_id" => $factor->id
            ]);
            $response = $controller->store($request);
            if($response["state"] == "failed"){
                return response()->json([
                    "message" => __("CompetitivenessMatrix.subFactor.created.failed")
                ], 400);
            }
        }

        return response()->json([
            "message" => __("CompetitivenessMatrix.factor.created.success")
        ]);

    }

    public function destroy($id)
    {
        Strength::where("factor_id", $id)->delete();
        Weaknesses::where("factor_id", $id)->delete();
        FactorSchoolScore::where("factor_id", $id)->delete();
        return parent::destroy($id);
    }

}
