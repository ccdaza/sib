<?php

namespace App\Http\Controllers;

use App\Models\Field;
use App\Models\Office;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class OfficeController extends Controller
{
    public function __construct()
    {
        $this->fileLang = "Offices";
        $this->name = "Oficinas";
        $this->setModel(Office::class);
        $this->setField(new Field("name", ["required"]));
        $this->setField(new Field("area_id", ["required"]));
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $offices = Office::orderBy("name", "asc")->with("area")->get();
        return $this->indexController($request, compact("offices"));
    }

    public function getJson(){
        $offices = Office::orderBy("name", "asc")->get();
        return response()->json($offices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return parent::store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return parent::update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
