<?php

namespace App\Http\Controllers;

use App\Area;
use App\Models\Budget\Activity;
use App\Models\RequestSupplies;
use App\Models\Budget\Resource;
use App\Models\Field;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class RequestSuppliesShoppingController extends Controller
{
    public function __construct()
    {
        $this->fileLang = "Areas";
        $this->name = "Solicitudes de insumos";
        $this->setModel(Activity::class);
        $this->setField(new Field("action", ["required"]));
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $requestSupplies = RequestSupplies::where("state", 0)->with(["resource", "category"])->get();
        return $this->indexController($request, compact("requestSupplies"));
    }

    public function updateCategoryJson(Request $request, $supply){
        $requestSupply = RequestSupplies::find($supply);
        $requestSupply->category_id = $request->category;
        $requestSupply->save();

        return response()->json(["message" => "Categoria enviada con éxito"]);
    }

}
