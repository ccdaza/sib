<?php

namespace App;

use App\Models\Dofa\UsersQuality;
use App\Models\Office;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = "users";
    }

    public function deleteTokens()
    {
        if($this->tokens->count() > 0){
            $this->tokens->map(function ($item){
                $item->delete();
            });
        }
    }

    public function canSee($module){
        $findModule = \App\Module::where("name", $module->name)->get();
        if($findModule->count() == 0){
            return false;
        }else{
            $module = $findModule->first();
            $findAction = \App\ModuleAction::where("module_id", $module->id)->where("name", "Ver")->get();
            if($findAction->count() == 0){
                return false;
            }else{
                $action = $findAction->first();
                $findPermission = ModuleActionUser::where("action_id", $action->id)->where("user_id", $this->id);
                if($findPermission->count() == 0){
                    return false;
                }else{
                    return true;
                }
            }
        }
    }

    public function canFinish($module){
        $findModule = \App\Module::where("name", $module->name)->get();
        if($findModule->count() == 0){
            return false;
        }else{
            $module = $findModule->first();
            $findAction = \App\ModuleAction::where("module_id", $module->id)->where("name", "Finalizar")->get();
            if($findAction->count() == 0){
                return false;
            }else{
                $action = $findAction->first();
                $findPermission = ModuleActionUser::where("action_id", $action->id)->where("user_id", $this->id);
                if($findPermission->count() == 0){
                    return false;
                }else{
                    return true;
                }
            }
        }
    }

    public function userQualify(){
        return $this->belongsTo(UsersQuality::class, "id", "user_id");
    }

    public function office(){
        return $this->belongsTo(Office::class, "office_id", "id");
    }
}
