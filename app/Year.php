<?php

namespace App;

use App\Models\Model;

class Year extends Model
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = "years";
    }
}
