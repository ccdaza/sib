<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome')->name("home");
Route::view('/mail', 'mail.DofaRequest');
Route::view('/responseGoogle', "authGoogle");
Route::get('responseIntranet', 'Auth\LoginController@responseIntranet');

Route::get('login/google', 'Auth\LoginController@loginWithGoogle');
Route::get('login/google/callback', 'Auth\LoginController@googleCallback');

Route::middleware('auth')->get('/user', function (Request $request) {
    return \Illuminate\Support\Facades\Auth::user();
});

Route::middleware('auth')->resource('categoriesShopping', "CategoriesShoppingController")->names("categoriesShopping");
Route::middleware('auth')->get('providers/finish/{provider}', "ProvidersController@finalizar")->name("providers.finalizar");
Route::middleware('auth')->get('providers/rut/{provider}', "ProvidersController@rut")->name("providers.rut");
Route::middleware('auth')->get('providers/camara/{provider}', "ProvidersController@camara")->name("providers.camara");
Route::middleware('auth')->post('providers/updateFiles/{provider}', "ProvidersController@updateFiles")->name("providers.updateFiles");
Route::post('providers/updateFiles2/{provider}', "ProvidersController@updateFiles2")->name("providers.updateFiles2");
Route::get('updateFiles/{hash}', "ProvidersController@updateFilesLink")->name("providers.updateFiles.link");

Route::middleware('auth')->resource('providers', "ProvidersController")->names("providers");

Route::middleware('auth')->get('/quoteFinish/{quote}', "QuotesController@finishQuote")->name("finishQuote");
Route::middleware('auth')->get('/quotes', "QuotesController@index")->name("quotes.index");


Route::prefix("Dofa")->namespace("Dofa")->group(function (){
    Route::get("/", "Controller@index");
    Route::post("/", "Controller@finish");
    Route::post("/usersRequest", "Controller@usersRequest");
});

Route::prefix("budget")->namespace("Budget")->group(function (){
    Route::view("/add", "Budget.add");
    Route::get("/addNewActivity", "Controller@addNewActivity")->name("budget.addNewActivity");
    Route::post("/newActivityExtra", "Controller@newActivityExtra")->name("budget.newActivityExtra");
    Route::get("/transfer", "Controller@transfer")->name("budget.transfer");
    Route::post("/transfer", "Controller@transfer")->name("budget.transfer");
});

Route::get("/UnapprovedActivities", "UnapprovedActivities@index")->name("UnapprovedActivities");
Route::get('probando', 'BalancedScorecard\Controller@index');
Route::get('avatar/{user}', 'UserController@avatar');

