<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/getUser', function (Request $request) {
    return $request->user();
});

Route::get('/years', function (Request $request) {
    return response()->json(\App\Year::orderBy("year", "desc")->get());
});
Route::middleware('auth')->get('/year', function (Request $request) {
    dd(session("year"));
});

Route::middleware('auth')->get("/users/find", "UserController@find");
Route::middleware('auth')->resource("/users", "UserController");
Route::middleware('auth')->get("/areas/json", "AreaController@getJson");
Route::middleware('auth')->resource("/areas", "AreaController");
Route::middleware('auth')->get("/offices/json", "OfficeController@getJson");
Route::middleware('auth')->resource("/offices", "OfficeController");
Route::middleware('auth')->resource("/centerCost", "CenterCostController");

Route::post('/login', 'Auth\LoginController@authenticate');
Route::middleware('client')->post('/intranetLogin', 'Auth\LoginController@intranetLogin');
Route::post('/setYear', 'Auth\LoginController@setYear');

Route::middleware('auth')->get('/notifications', 'NotificationController@getNotifications');

Route::get('/page/{page}', function (Request $request){
    return response()->json([
        "canSee" => $request->user()->canSee("modulo")
    ]);
})->middleware('auth');

Route::put('/user/update', 'UserController@update')->middleware('auth');
Route::post('/avatar', 'UserController@updateAvatar')->middleware('auth');

Route::middleware('auth')->prefix("CompetitivenessMatrix")->namespace("CompetitivenessMatrix")->group(function (){
    Route::get("/", "Controller@index");
    Route::post("/", "Controller@finish");
    Route::resource("factors", "FactorController");
    Route::resource("school", "SchoolController");
    Route::resource("factorSchoolScore", "FactorSchoolScoreController");
});

Route::middleware('auth')->prefix("Pestel")->namespace("Pestel")->group(function (){
    Route::get("/", "Controller@index");
    Route::post("/", "Controller@finish");
    Route::resource("aspect", "AspectController");
});

Route::middleware('auth')->prefix("Dofa")->namespace("Dofa")->group(function (){
    Route::get("/", "Controller@index");
    Route::post("/", "Controller@finish");
    Route::post("/usersRequest", "Controller@usersRequest");
});

Route::middleware('auth')->prefix("DofaScore")->namespace("Dofa")->group(function (){
    Route::get("/", "Controller@indexScore");
});

Route::middleware('auth')->prefix("DofaScoreUser")->group(function (){
    Route::post("/", "DofaScoreUserController@store");
});

Route::middleware('auth')->prefix("StrategySummary")->namespace("StrategySummary")->group(function (){
    Route::get("/", "Controller@index");
    Route::post("/", "Controller@store");
    Route::post("/finish", "Controller@finish");
});

Route::middleware('auth')->prefix("InterestGroups")->namespace("InterestGroups")->group(function (){
    Route::get("/", "Controller@index");
    Route::post("/", "Controller@store");
    Route::post("/finish", "Controller@finish");
    Route::post("/addGestion", "Controller@addGestion");
});

Route::middleware('auth')->prefix("StrategicIssuesMatrix")->namespace("StrategicIssuesMatrix")->group(function (){
    Route::get("/", "Controller@index");
    Route::post("/", "Controller@store");
    Route::post("/finish", "Controller@finish");
    Route::post("/score", "Controller@score");
    Route::post("/usersRequest", "Controller@usersRequest");
});

Route::middleware('auth')->prefix("StrategicIssuesMatrixScore")->namespace("StrategicIssuesMatrix")->group(function (){
    Route::get("/", "Controller@indexScore");
    Route::post("/score", "Controller@score");
});

Route::middleware('auth')->prefix("StrategicIssues")->namespace("StrategicIssues")->group(function (){
    Route::get("/", "Controller@index");
    Route::post("/", "Controller@store");
    Route::post("/finish", "Controller@finish");
    Route::post("/saveObjective", "Controller@saveObjective");
});

Route::middleware('auth')->prefix("BalancedScorecard")->namespace("BalancedScorecard")->group(function (){
    Route::get("/", "Controller@index");
    Route::post("/", "Controller@store");
    Route::put("/{balanced}", "Controller@update");
    Route::post("/finish", "Controller@finish");
});

Route::middleware('auth')->prefix("Budget")->namespace("Budget")->group(function (){
    Route::get("/", "Controller@index");
    Route::post("/{budget}/activity", "Controller@storeActivity");
    Route::delete("/activity/{activity}", "Controller@destroyActivity");
    Route::delete("/resource/{resource}", "Controller@destroyResource");
    Route::put("/resource/{resource}", "Controller@setAccount");
    Route::post("/finish", "Controller@finish");
    //approve
    Route::get("/Approve", "ApproveController@index");
    Route::post("/Approve/finish", "ApproveController@finish");
    Route::post("/Approve/{budget}", "ApproveController@approve");
    Route::post("/Refuse/{budget}", "ApproveController@refuse");
    Route::post("/ApproveExtra/{budget}", "ApproveController@approveExtra");
    Route::post("/RefuseExtra/{budget}", "ApproveController@refuseExtra");
    Route::post("/Approve/Area/{area}", "ApproveController@approveByArea");
    Route::post("/Refuse/Area/{area}", "ApproveController@refuseByArea");
    Route::post("/ApproveExtra/Area/{area}", "ApproveController@approveExtraByArea");
    Route::post("/RefuseExtra/Area/{area}", "ApproveController@refuseExtraByArea");
    //approved
    Route::get("/Approved", "ApprovedController@index");
    Route::post("/Approved/request/{activity}", "ApprovedController@request");
});


Route::middleware('auth')->prefix("CashFlowRequests")->namespace("CashFlowRequests")->group(function () {
    Route::get("/", "Controller@index");
    Route::post("/approve/{request}", "Controller@approve");
    Route::post("/refuse/{request}", "Controller@refuse");
});

Route::middleware('auth')->get("/requestSupplies/{activity}", "RequestSuppliesController@index");
Route::middleware('auth')->post("/requestSupplies/{activity}", "RequestSuppliesController@request");

Route::middleware('auth')->prefix("SetAccounts")->namespace("Budget")->group(function () {
    Route::get("/", "SetAccountsController@index");
    Route::put("/{resource}", "SetAccountsController@update");
    Route::post("/finish", "SetAccountsController@finish");
});

Route::middleware('auth')->get("/requestSuppliesShopping", "RequestSuppliesShoppingController@index");

Route::middleware('auth')->resource("/Years", "YearsController");

Route::middleware('auth')->get("/categoriesShoppingJson", "CategoriesShoppingController@json");
Route::middleware('auth')->post("/categoriesShopping/create", "CategoriesShoppingController@create");
Route::middleware('auth')->post("/categoriesShopping/update/{supply}", "RequestSuppliesShoppingController@updateCategoryJson");

Route::middleware('auth')->post("/quote/create/{supply}", "QuotesController@create");

Route::middleware('auth')->get("/modulesWithActions", function (){
    $modules = \App\Module::orderBy("name", "asc")->with("actions")->get();
    return response()->json($modules);
});

Route::middleware('auth')->post("/canSee", function (Request $request){
    $response = [
        "can" => false
    ];
    $module = \App\Module::where("name", $request->get("module"))->get();
    if($module->count() > 0){
        $action = \App\ModuleAction::where("name", "Ver")->where("module_id", $module->last()->id)->get();
        if($action->count() > 0){
            $moduleUser = \App\ModuleActionUser::where("action_id", $action->last()->id)->where("user_id", $request->user()->id)->get();
            if($moduleUser->count() > 0){
                $response["can"] = true;
            }
        }
    }
    return response()->json($response);
});

Route::middleware('auth')->post("/presupuestoAprobado", "Budget\ApproveController@totalByArea");
Route::middleware('auth')->post("/presupuestoAprobadoPorMes/all", "Budget\ApproveController@totalByAreaAndMonths");
Route::middleware('auth')->post("/presupuestoAprobadoPorMes/{month?}", "Budget\ApproveController@totalByAreaAndMonth");

Route::middleware('auth')->get("/permissionsUser/{user}", function (Request $request, $user){
    $permissions = \App\ModuleActionUser::select("action_id")->where("user_id", $user)->get();
    return response()->json($permissions);
});


Route::middleware('auth')->get("/providers", "ProvidersController@listJson");
Route::middleware('auth')->post("/quotes/{quote}/setProvider/{number}", "QuotesController@setProvider");
Route::middleware('auth')->post("/quotes/{quote}/setValor/{number}", "QuotesController@setValor");
Route::middleware('auth')->post("/quotes/{quote}/setCarac/{number}", "QuotesController@setCarac");
Route::middleware('auth')->post("/quotes/{quote}/setCV/{number}", "QuotesController@setCv");
Route::middleware('auth')->post("/quotes/{quote}/setPrecio/{number}", "QuotesController@setPrecio");
Route::middleware('auth')->post("/quotes/{quote}/setWinner", "QuotesController@setWinner");
