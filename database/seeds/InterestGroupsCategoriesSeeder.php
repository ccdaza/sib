<?php

use Illuminate\Database\Seeder;

class InterestGroupsCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            "Alta dirección",
            "Beneficiarios",
            "Competidores",
            "Empleados Y Trabajadores",
            "Entidades Financieras",
            "Entidades Gubernamentales",
            "Entorno Social",
            "Medio Ambiente",
            "Medios De Comunicación",
            "Propiedad",
            "Proveedores Y Subcontratistas",
            "Redes Y Convenio"
        ];

        foreach ($categories as $categoryName){
            $category = new \App\Models\InterestGroups\Category();
            $category->name = $categoryName;
            $category->save();
        }
    }
}
