<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            "name" => "prueba",
            "email" => "prueba@prueba.com",
            "password" => bcrypt("123456")
        ]);

        $actions = \App\ModuleAction::all();

        foreach ($actions as $action){
            $actionUser = new \App\ModuleActionUser();
            $actionUser->action_id = $action->id;
            $actionUser->user_id = $user->id;
            $actionUser->save();
        }

    }
}
