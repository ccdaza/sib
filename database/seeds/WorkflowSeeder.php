<?php

use Illuminate\Database\Seeder;

class WorkflowSeeder extends Seeder
{
    public static $tasks = [
        \App\Http\Controllers\CompetitivenessMatrix\Controller::class,
        \App\Http\Controllers\Pestel\Controller::class,
        \App\Http\Controllers\Dofa\Controller::class,
        \App\Http\Controllers\StrategySummary\Controller::class,
        \App\Http\Controllers\InterestGroups\Controller::class,
        \App\Http\Controllers\StrategicIssuesMatrix\Controller::class,
        \App\Http\Controllers\StrategicIssues\Controller::class,
        \App\Http\Controllers\BalancedScorecard\Controller::class,
        \App\Http\Controllers\Budget\Controller::class,
        \App\Http\Controllers\Budget\ApproveController::class,
        \App\Http\Controllers\Budget\ApprovedController::class,
        \App\Http\Controllers\Budget\SetAccountsController::class,
        \App\Http\Controllers\CashFlowRequests\Controller::class,
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach (\App\Year::all() as $year){
            foreach (self::$tasks as $task){
                $workflow = new \App\Workflow();
                $task = new $task();
                if(!is_null($task->task)){
                    continue;
                }
                $workflow->task = $task->name;
                try{
                    $workflow->save();
                }catch (\Exception $e){
                    continue;
                }
            }

            foreach (self::$tasks as $task){
                $task = new $task();
                foreach ($task->depends as $depend){
                    $depend = new $depend();
                    if(!is_null($depend->task) && count($depend->depends) > 0){
                        $workflowDepends = new \App\WorkflowDepends();
                        $workflowDepends->task_id = $task->task->id;
                        $workflowDepends->depends_task_id = $depend->task->id;
                        try{
                            $workflowDepends->save();
                        }catch (\Exception $e){
                            continue;
                        }
                    }
                }
            }


        }

    }
}
