<?php

use Illuminate\Database\Seeder;

class FrequencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $frequencies = [
            "Mensual",
            "Bimensual",
            "Trimensual",
            "Semestral",
            "Anual",
        ];

        foreach ($frequencies as $frequency){
            $frequencyObject = new \App\Models\Frequency();
            $frequencyObject->name = $frequency;
            $frequencyObject->save();
        }

    }
}
