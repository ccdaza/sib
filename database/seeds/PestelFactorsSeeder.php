<?php

use Illuminate\Database\Seeder;

class PestelFactorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            "PÓLITICO",
            "ECONOMICOS",
            "SOCIOCULTURALES",
            "TECNOLOGICO",
            "ECOLOGICO",
            "LEGISLATIVO"
        ];

        foreach ($types as $type){
            $factor = new \App\Models\Pestel\Factor();
            $factor->name = $type;
            $factor->save();
        }

    }
}
