<?php

use Illuminate\Database\Seeder;

class ModulesActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = [
            App\Http\Controllers\CompetitivenessMatrix\Controller::class,
            App\Http\Controllers\StrategicIssuesMatrix\Controller::class,
            App\Http\Controllers\Pestel\Controller::class,
            App\Http\Controllers\Dofa\Controller::class,
            App\Http\Controllers\StrategySummary\Controller::class,
            App\Http\Controllers\InterestGroups\Controller::class,
            App\Http\Controllers\StrategicIssues\Controller::class,
            App\Http\Controllers\BalancedScorecard\Controller::class,
            App\Http\Controllers\Budget\Controller::class,
            \App\Http\Controllers\YearsController::class,
            \App\Http\Controllers\UserController::class,
            \App\Http\Controllers\Budget\ApproveController::class,
            \App\Http\Controllers\Budget\ApprovedController::class,
            \App\Http\Controllers\AreaController::class,
            \App\Http\Controllers\OfficeController::class,
            \App\Http\Controllers\CashFlowRequests\Controller::class,
            \App\Http\Controllers\RequestSuppliesController::class,
            \App\Http\Controllers\RequestSuppliesShoppingController::class,
        ];

        foreach ($modules as $module){
            $module = new $module();
            $response = \App\Module::where("name", $module->name)->get();
            if($response->count() > 0){
                $moduleRegister = $response->first();
            }else{
                $moduleRegister = new App\Module();
                $moduleRegister->name = $module->name;
                try{
                    $moduleRegister->save();
                }catch (\Exception $e){
                    dump($e->getMessage());
                }
            }

            foreach ($module->actions as $action){
                $responseAction = \App\ModuleAction::where("module_id", $moduleRegister->id)->where("name", $action)->get();
                if($responseAction->count() > 0){
                    $actionRegister = $responseAction->first();
                }else {
                    $actionRegister = new \App\ModuleAction();
                    $actionRegister->module_id = $moduleRegister->id;
                    $actionRegister->name = $action;
                    try {
                        $actionRegister->save();
                    } catch (\Exception $e) {
                        dump($e->getMessage());
                    }
                }
            }

        }
    }
}
