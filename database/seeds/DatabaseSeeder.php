<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(YearTableSeeder::class);
        $this->call(SchoolSeeder::class);
        $this->call(WorkflowSeeder::class);
        $this->call(PestelFactorsSeeder::class);
        $this->call(ModulesActionSeeder::class);
        $this->call(StrategicIssuesPerspectiveSeeder::class);
        $this->call(InterestGroupsCategoriesSeeder::class);
        $this->call(FrequencySeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}
