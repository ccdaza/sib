<?php

use Illuminate\Database\Seeder;

class StrategicIssuesCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            "Comunicación Organizacional",
            "Alianzas estratégicas",
            "Bienestar laboral",
            "Cuidado de la casa común",
            "Formación Integral y Calidad Educativa",
            "Formación y desarrollo de compañeros apostolicos",
            "Fortalecer Calidad Educativa",
            "Marketing y Mercadeo",
            "Sostenibilidad Financiera",
            "Vanguardia Tecnológica"
        ];

        foreach ($categories as $category){
            $strategicIssuesCategory = new \App\Models\StrategicIssues\Category();
            $strategicIssuesCategory->name = $category;
            $strategicIssuesCategory->save();
        }
    }
}
