<?php

use Illuminate\Database\Seeder;

class StrategicIssuesPerspectiveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $perspectives = [
            "Aprendizaje y crecimiento",
            "Beneficiarios",
            "Financiera",
            "Procesos Internos"
        ];

        foreach ($perspectives as $perspective){
            $perspectiveObject = new \App\Models\StrategicIssues\Perspective();
            $perspectiveObject->name = $perspective;
            $perspectiveObject->save();
        }
    }
}
