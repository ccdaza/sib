<?php

use Illuminate\Database\Seeder;

class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $school = new \App\Models\CompetitivenessMatrix\School();
        $school->name = "San Bartolome";
        $school->save();
    }
}
