<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupsTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'interest_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->boolean('power');
            $table->boolean('interest');
            $table->integer('category_id');
            $table->boolean('required')->default(false);
            $table->integer('need_id');
            $table->integer('expectation_id');
            $table->integer('area_id');
            $table->boolean('risk')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'interest_groups');
    }
}
