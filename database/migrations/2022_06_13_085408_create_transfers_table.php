<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'transfers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('area_id');
            $table->text('accion');
            $table->integer('ejecutor_id');
            $table->bigInteger('valor');
            $table->integer('area_saldo_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'request_supplies');
    }
}
