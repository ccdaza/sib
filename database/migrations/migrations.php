<?php

namespace database\migrations;

use App\Workflow;
use App\WorkflowDepends;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;
use mysql_xdevapi\Exception;

class migrations{

    static function newDB($year){

        Session::put("year", $year);

        $tables = [
            new \CreateFactorsTable($year),
            new \CreateWeaknessesTable($year),
            new \CreateStrengthsTable($year),
            new \CreateSchoolsTable($year),
            new \CreateFactorSchoolScoresTable($year),
            new \CreateAspectsTable($year),
            new \CreateThreatsTable($year),
            new \CreateOpportunitiesTable($year),
            new \CreateStrengthOpportunitiesTable($year),
            new \CreateStrengthsThreatsTable($year),
            new \CreateWeaknessesThreatsTable($year),
            new \CreateWeaknessesOpportunitiesTable($year),
            new \CreateStrengthsOpportunitiesScoreUsersTable($year),
            new \CreateStrengthsThreatsScoreUsersTable($year),
            new \CreateWeaknessesOpportunitiesScoreUsersTable($year),
            new \CreateWeaknessesThreatsScoreUsersTable($year),
            new \CreateUsersQualitiesTable($year),
            new \CreateStrategicIssuesCategoriesTable($year),
            new \CreateStrategiesTable($year),
            new \CreateStrategyIssuesTable($year),
            new \CreateNeedsTable($year),
            new \CreateExpectationsTable($year),
            new \CreateGroupsTable($year),
            new \CreateStrategicIssuesMatrixScoresTable($year),
            new \CreateObjectivesTable($year),
            new \CreateStrategicIndicatorsTable($year),
            new \CreateBalancedScorecardsTable($year),
            new \CreatePerspectiveBalancedScorecardsTable($year),
            new \CreateBudgetsTable($year),
            new \CreateActivitiesTable($year),
            new \CreateResourcesTable($year),
            new \CreateRequestMoneyTable($year),
            new \CreateWorkflowsTable($year),
            new \CreateWorkflowDependsTable($year),
            new \CreateQuoteProviderTable($year),
            new \CreateQuotesTable($year),
            new \CreateRequestSuppliesTable($year),
            new \CreateActivitiesExtraTable($year),
            new \CreateActivitiesExtraActivitiesTable($year),
            new \CreateActivitiesExtraResourcesTable($year),
            new \CreateStrategicIssuesMatrixUsersTable($year),
            new \CreateGestionInterestGroupsNeedsTable($year),
        ];

        $tablesReady = [];

        foreach ($tables as $index => $table){
            try{
                $table->up();
            }catch (\Exception $exception){
                foreach ($tablesReady as $_table){
                    $_table->down();
                }
                throw $exception;
            }
            $tablesReady[] = $table;
        }

        foreach (\WorkflowSeeder::$tasks as $task){
            $workflow = new Workflow();
            $task = new $task();
            if(!is_null($task->task)){
                continue;
            }

            $workflow->task = $task->name;
            try{
                $workflow->save();
            }catch (\Exception $e){
                continue;
            }
            foreach ($task->depends as $depend){
                $depend = new $depend();
                $workflowDepends = new WorkflowDepends();
                $workflowDepends->task_id = $workflow->id;
                $workflowDepends->depends_task_id = $depend->task->id;
                try{
                    $workflowDepends->save();
                }catch (\Exception $e){
                    continue;
                }
            }
        }

        $seeder = new \SchoolSeeder();
        $seeder->run();

        $seeder = new \StrategicIssuesCategoriesSeeder();
        $seeder->run();

        $seeder = new \StrategicIssuesPerspectiveSeeder();
        $seeder->run();

        $seeder = new \InterestGroupsCategoriesSeeder();
        $seeder->run();


    }

}
