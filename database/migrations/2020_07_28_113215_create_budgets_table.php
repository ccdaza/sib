<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateBudgetsTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'budgets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('objective_id');
            $table->integer('strategy_id');
            $table->integer('area_id');
            $table->tinyInteger('state')->default(0); // 0 - en espera, 1 - aprobado, 2 - rechazado
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'budgets');
    }
}
