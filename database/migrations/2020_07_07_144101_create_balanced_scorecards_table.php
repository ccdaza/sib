<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateBalancedScorecardsTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'balanced_scorecards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('formula');
            $table->integer('desired_goal');
            $table->integer('acceptable_goal');
            $table->integer('minimum_goal');
            $table->integer('measurement_frequency_id');
            $table->integer('evaluation_frequency_id');
            $table->string('measurement_source');
            $table->integer('responsible_measurement_id');
            $table->integer('head_analysis_id');
            $table->integer('area_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'balanced_scorecards');
    }
}
