<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesExtraTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'activities_extra', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('area_id');
            $table->text('ref');
            $table->tinyInteger('state')->default(0); // 0 - en espera, 1 - aprobado, 2 - rechazado
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'request_supplies');
    }
}
