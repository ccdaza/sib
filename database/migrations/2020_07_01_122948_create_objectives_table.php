<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateObjectivesTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'strategic_issues_categories_objectives', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('objective');
            $table->integer('perspective_id');
            $table->string('premise');
            $table->integer('category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'strategic_issues_categories_objectives');
    }
}
