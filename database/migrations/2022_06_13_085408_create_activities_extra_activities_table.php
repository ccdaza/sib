<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesExtraActivitiesTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'activities_extra_activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('activities_extra_id');
            $table->integer('office_id');
            $table->text('action');
            $table->text('description');
            $table->integer('responsable_id');
            $table->integer('executor_id');
            $table->integer('start_date');
            $table->integer('end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'request_supplies');
    }
}
