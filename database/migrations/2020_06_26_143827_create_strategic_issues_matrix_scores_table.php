<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateStrategicIssuesMatrixScoresTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'strategic_issues_matrix_scores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('strategic_issues_category_id');
            $table->string('type');
            $table->integer('data_id');
            $table->integer('score');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'strategic_issues_matrix_scores');
    }
}
