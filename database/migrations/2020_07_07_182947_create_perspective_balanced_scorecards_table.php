<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreatePerspectiveBalancedScorecardsTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'objectives_balanced_scorecards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('objective_id');
            $table->integer('strategy_id');
            $table->integer('balanced_scorecard_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'objectives_balanced_scorecards');
    }
}
