<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesExtraResourcesTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'activities_extra_resources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('activity_id');
            $table->text('description');
            $table->integer('quantity');
            $table->string('unit');
            $table->integer('value');
            $table->integer('account');
            $table->integer('center_cost_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'request_supplies');
    }
}
