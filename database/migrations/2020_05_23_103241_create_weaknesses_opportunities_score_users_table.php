<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateWeaknessesOpportunitiesScoreUsersTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'weaknesses_opportunities_score_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_qualify_id');
            $table->integer('score');
            $table->integer('weakness_opportunity_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'weaknesses_opportunities_score_users');
    }
}
