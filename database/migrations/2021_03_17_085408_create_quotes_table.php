<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'quotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('request_supply_id')->unique();
            $table->integer('provider1_id')->nullable();
            $table->json('provider1_ratings')->nullable();
            $table->integer('provider2_id')->nullable();
            $table->json('provider2_ratings')->nullable();
            $table->integer('provider3_id')->nullable();
            $table->json('provider3_ratings')->nullable();
            $table->smallInteger('winner')->nullable();
            $table->boolean('state')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'quotes');
    }
}
