<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateAspectsTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'pestel_aspects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('factor_id');
            $table->boolean('impact');
            $table->smallInteger('intensity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'pestel_aspects');
    }
}
