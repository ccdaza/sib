<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateStrengthsThreatsScoreUsersTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'strengths_threats_score_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_qualify_id');
            $table->integer('score');
            $table->integer('strength_threat_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'strengths_threats_score_users');
    }
}
