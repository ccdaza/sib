<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGestionInterestGroupsNeedsTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'gestion_interest_groups_needs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('interest_groups_need_id');
            $table->text('gestion');
            $table->text('file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'gestion_interest_groups_needs');
    }
}
