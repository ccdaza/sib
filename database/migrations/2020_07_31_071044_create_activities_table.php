<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateActivitiesTable extends \App\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefix . 'budget_activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('budget_id');
            $table->integer('office_id');
            $table->text('action');
            $table->text('description');
            $table->integer('responsible_id');
            $table->integer('executor_id');
            $table->integer('start_date');
            $table->integer('end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefix . 'budget_activities');
    }
}
