<?php

return [
    "created" => [
        "success" => "Grupo de interes creado con exito",
        "failed" => "No fue posible crear el grupo de interes"
    ],
    "finish" => [
        "success" => "Tarea finializada con exito",
        "failed" => "No fue posible finalizar la tarea"
    ]
];
