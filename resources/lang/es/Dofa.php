<?php

return [
    "usersQualify" => [
        "created" => [
            "success" => "Solicitudes enviadas con exito"
        ]
    ],
    "finish" => [
        "success" => "Tarea finializada con exito",
        "failed" => "No fue posible finalizar la tarea"
    ]
];
