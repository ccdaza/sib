<?php

return [
    "aspect" => [
        "factor" => [
            "required" => "El campo factor es obligatorio"
        ],
        "dofa" => [
            "required" => "El campo DOFA es obligatorio"
        ],
        "impact" => [
            "required" => "El campo impacto es obligatorio"
        ],
        "intensity" => [
            "required" => "El campo intensidad es obligatorio"
        ],
        "created" => [
            "success" => "Aspecto creado con exito",
            "failed" => "No fue posible crear el nuevo aspecto"
        ],
        "update" => [
            "success" => "Aspecto actualizado con exito",
            "failed" => "No fue posible actualizar el aspecto"
        ],
        "deleted" => [
            "success" => "Aspecto eliminado con exito",
            "failed" => "No fue posible eliminar el factor"
        ]
    ],
    "threats" => [
        "name" => [
            "required" => "El campo aspecto a mejorar es obligatorio"
        ],
        "created" => [
            "success" => "Amenaza creada con exito",
            "failed" => "No fue posible crear la nueva amenaza"
        ],
        "update" => [
            "success" => "Amenaza actualizada con exito",
            "failed" => "No fue posible actualizar la amenaza"
        ],
        "deleted" => [
            "success" => "Amenaza eliminada con exito",
            "failed" => "No fue posible eliminar la amenaza"
        ]
    ],
    "opportunities" => [
        "name" => [
            "required" => "El campo aspecto a mejorar es obligatorio"
        ],
        "created" => [
            "success" => "Oportunidad creada con exito",
            "failed" => "No fue posible crear la nueva pportunidad"
        ],
        "update" => [
            "success" => "Oportunidad actualizada con exito",
            "failed" => "No fue posible actualizar la oportunidad"
        ],
        "deleted" => [
            "success" => "Oportunidad eliminada con exito",
            "failed" => "No fue posible eliminar la oportunidad"
        ]
    ],
    "finish" => [
        "success" => "Tarea finializada con exito",
        "failed" => "No fue posible finalizar la tarea"
    ]
];
