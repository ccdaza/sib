<?php

return [
    "created" => [
        "success" => "Creada con exito",
        "failed" => "No fue posible"
    ],
    "update" => [
        "success" => "Aactualizada con exito",
        "failed" => "No fue posible actualizar"
    ],
];
