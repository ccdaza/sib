<?php

return [
    "factor" => [
        "name" => [
            "required" => "El campo nombre es obligatorio"
        ],
        "weighing" => [
            "required" => "El campo ponderración es obligatorio"
        ],
        "created" => [
            "success" => "Factor creado con exito",
            "failed" => "No fue posible crear el nuevo factor"
        ],
        "update" => [
            "success" => "Factor actualizado con exito",
            "failed" => "No fue posible actualizar el factor"
        ],
        "deleted" => [
            "success" => "Factor eliminado con exito",
            "failed" => "No fue posible eliminar el factor"
        ]
    ],
    "subFactor" => [
        "created" => [
            "success" => "Subfactor creado con exito",
            "failed" => "No fue posible crear el nuevo subfactor"
        ]
    ],
    "strength" => [
        "created" => [
            "failed" => "No fue posible crear la fortaleza",
            "success" => "Fortaleza creada con exito"
        ]
    ],
    "weakness" => [
        "created" => [
            "failed" => "No fue posible crear la debilidad",
            "success" => "Debilidad creada con exito"
        ]
    ],
    "school" => [
        "name" => [
            "required" => "El campo nombre es obligatorio"
        ],
        "created" => [
            "failed" => "No fue posible crear el colegio",
            "success" => "Colegio creado con exito"
        ],
        "update" => [
            "success" => "Colegio actualizado con exito",
            "failed" => "No fue posible actualizar el colegio"
        ],
        "deleted" => [
            "success" => "Colegio eliminado con exito",
            "failed" => "No fue posible eliminar el colegio"
        ]
    ],
    "FactorSchoolScore" => [
        "created" => [
            "failed" => "No fue posible asignar la calificación",
            "success" => "Calificación asignada con exito"
        ],
        "update" => [
            "success" => "Calificación actualizada con exito",
            "failed" => "No fue posible actualizar la calificación"
        ],
        "deleted" => [
            "success" => "Calificación eliminada con exito",
            "failed" => "No fue posible eliminar la calificación"
        ]
    ],
    "finish" => [
        "success" => "Tarea finializada con exito",
        "failed" => "No fue posible finalizar la tarea"
    ]
];
