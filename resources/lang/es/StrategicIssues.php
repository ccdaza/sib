<?php

return [
    "created" => [
        "success" => "Creada con exito",
        "failed" => "No fue posible"
    ],
    "update" => [
        "success" => "Aactualizada con exito",
        "failed" => "No fue posible actualizar"
    ],
    "objectives" => [
        "created" => [
            "success" => "Creada con exito",
            "failed" => "No fue posible crear el objetivo, por favor intentelo mas tarde"
        ],
    ],
    "finish" => [
        "success" => "Tarea finializada con exito",
        "failed" => "No fue posible finalizar la tarea"
    ]
];
