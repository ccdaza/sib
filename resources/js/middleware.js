export default {
    auth: (to, from, next) => {
        if(!localStorage.autenticate_token){
            next({ name: "Login" });
        }else{
            next()
        }
    },
    http401(router){
        localStorage.clear();
        router.push("/");
    }
}
