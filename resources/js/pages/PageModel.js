import Axios from "axios";
import Middleware from "../middleware";

import Pages from "../components/Pages";
import Alert from "../components/Alert"
import Button from "../components/Alert/Button"

let button1 = new Button();
button1.setDisable(true);

export default{
    data(){
        return {
            page: {
                state: 0,
                finalized: false,
                depends: []
            },
            PAGE_STATE: {
                loading: 0,
                complete: 1,
                failed: 2,
                notAuthorized: 3,
                dependenciesNotFinalized: 4
            },
            axiosConfig: {
                headers: {
                    Accept: 'application/json',
                    Authorization: 'Bearer ' + localStorage.autenticate_token
                }
            },
            confirmFinish: false,
            alert: {
                title: "",
                message: "",
                type: "success",
                show: false,
                button1: button1
            },
        }
    },
    components: {
        Pages,
        Alert
    },
    computed: {
        authorized() {
            return this.page.state == this.PAGE_STATE.complete;
        }
    },
    methods: {
        getPage(){
            Axios.get("/api/" + this.$options.name, this.axiosConfig)
                .then(response => {
                    this.$set(this.page, "state", response.data.finishedDependencies ? response.data.canSee ? this.PAGE_STATE.complete : this.PAGE_STATE.notAuthorized : this.PAGE_STATE.dependenciesNotFinalized);
                    this.$set(this.page, "finalized", response.data.finalized);
                    this.$set(this.page, "depends", response.data.depends);
                    if(this.page.state == this.PAGE_STATE.complete){
                        this.afterComplete(response.data);
                    }
                    console.log("pagemodel", this.page.state);
                })
                .catch(error => {
                    this.errorGetPage(error);
                })
        },
        errorGetPage(error){
            if(error.response.status == 401){
                Middleware.http401(this.$router);
            }else if(error.response.status == 400){
                this.$set(this.page, "state", this.PAGE_STATE.notAuthorized);
            }else{
                this.$set(this.page, "state", this.PAGE_STATE.failed);
            }
        },
        pageChanges(data){
            const keys = data.getAllKeys();
            for(let key in keys){
                this.$set(this.page, keys[key], data[keys[key]]);
            }
        },
        afterComplete(data){
            return true;
        },
        finishTask(){
            if(this.confirmFinish){
                Axios.post("/api/" + this.$options.name + '/finish', {}, this.axiosConfig)
                    .then(response => {
                        this.showAlert({
                            title: "Operación culminada con éxito",
                            message: response.data.message,
                            type: "success"
                        });
                        this.page.finalized = true;
                    }).catch(error => {
                        let button1 = new Button();
                        button1.setDisable(true);
                        button1.setText("Aceptar");
                        this.showAlert({
                            title: "la operación no se pudo completar",
                            message: error.response.data.message,
                            type: "danger"
                        });
                        this.confirmFinish = false;
                    })
            }else{
                let btn = new Button();
                btn.setText("Finalizar");
                btn.setOnClick(() => {
                    this.confirmFinish = true;
                    this.finishTask();
                })

                this.showAlert({
                    title: "¿Esta seguro?",
                    message: "¿Desea terminar este proceso?",
                    type: "info",
                    button: btn
                })
            }
        },
        showAlert(data){
            this.alert.title = data.title;
            this.alert.message = data.message;
            this.alert.type = data.type;
            if(data.button != undefined){
                this.alert.button1 = data.button;
            }else{
                this.alert.button1 = button1;
            }
            this.alert.show = true;
        },
        awaiting(){
            console.log("await")
            this.page.state = 0;
        },
        resume(){
            console.log("resumen");
            this.page.state = 1;
        }
    },
    watch: {
        page(value){
            console.log("pagina");
        }
    },
    mounted() {
        this.getPage();
    }
}
