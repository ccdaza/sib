import Vue from 'vue'
import Index from './Index'
import router from './Route'

Vue.config.warnHandler = null;

new Vue({
    el: '#app',
    router,
    components: { Index },
    template: '<Index/>'
})
