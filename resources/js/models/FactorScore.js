export default class FactorScore{

    constructor(id, factor_id, school_id, score){
        this.id = id;
        this.factor_id = factor_id;
        this.school_id = school_id;
        this.score = score;
    }

    setId(value){
        this.id = value;
    }

    setFactorId(value){
        this.factor_id = value;
    }

    setSchoolId(value){
        this.school_id = value;
    }

    setScore(value){
        this.score = value;
    }
}
