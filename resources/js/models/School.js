import FactorScore from "./FactorScore";

export default class School {

    constructor(id, name, factors_score){
        this.id = id;
        this.name = name;
        this.factors_score = [];
        this.setFactorsScore(factors_score);
    }

    setId(value){
        this.id = value;
    }

    setName(value){
        this.name = value;
    }

    setFactorsScore(value){
        for(let index in value){
            let factorScore = new FactorScore(value[index].id, value[index].factor_id, value[index].school_id, value[index].score);
            this.factors_score.push(factorScore);
        }
    }
}
