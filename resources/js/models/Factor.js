export default class Factor{

    constructor(id, name, weighing, subFactors){
        this.id = id;
        this.name = name;
        this.weighing = weighing;
        this.subFactors = subFactors;
    }

    setId(value){
        this.id = value;
    }

    setName(value){
        this.name = value;
    }

    setWeighing(value){
        this.weighing = value;
    }

    setSubFactors(value){
        this.subFactors = value;
    }
}
