export default class Button {

    constructor(){
        this.type = "btn-primary";
        this.text = "Boton";
        this.disable = false;
        this.visible = true;
        this.onClick = () => {};
    }

    setType(type){
        this.type = type;
    }

    setText(text){
        this.text = text;
    }

    setDisable(disable){
        this.disable = disable;
    }

    setVisible(visible){
        this.visible = visible;
    }

    setOnClick(onClick){
        this.onClick = onClick;
    }

}
