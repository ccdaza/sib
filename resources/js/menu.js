export default {
    principal: [
        {
            "title" : "Dashboard",
            "icon" : "dashboard",
            "href" : "/",
        },
        {
            "title" : "Planeación estrategica",
            "icon" : "timeline",
            "subs" : [
                {
                    "title" : "Matriz perfil de competitividad",
                    "href" : "/competitivenessMatrix",
                    "permission" : true
                },
                {
                    "title" : "Pestel",
                    "href" : "/pestel"
                },
                {
                    "title" : "Dofa",
                    "href" : "/dofa"
                },
                {
                    "title" : "Resumen de estrategias",
                    "href" : "/strategySummary"
                },
                {
                    "title" : "Grupos de interes",
                    "href" : "/interestGroups"
                },
                {
                    "title" : "Matriz de cobertura de los temas estrategicos",
                    "href" : "/strategicIssuesMatrix"
                },
                {
                    "title" : "Temas estrategicos",
                    "href" : "/strategicIssues"
                },
                {
                    "title" : "Cuadro de mando integral",
                    "href" : "/balancedScorecard"
                },
            ]
        },
        {
            "title" : "POA",
            "icon" : "timeline",
            "subs" : [
                {
                    "title" : "Solicitud",
                    "href" : "/budget/request"
                },
                {
                    "title" : "Aprobación",
                    "href" : "/budget/approve"
                },
                {
                    "title" : "Aprobado",
                    "href" : "/budget/approved"
                },
                {
                    "title" : "Adicción de presupuesto",
                    "href" : "/budget/add",
                    "simple" : true
                },
            ]
        },
        {
            "title" : "Financiera",
            "icon" : "timeline",
            "subs" : [
                {
                    "title" : "Asignación de cuentas",
                    "href" : "/setAccounts"
                },
                {
                    "title" : "Solicitudes de flujo de caja",
                    "href" : "/cashFlowRequest"
                },
                {
                    "title" : "Traslado de presupuesto",
                    "href" : "/budget/transfer",
                    "simple" : true
                },
            ]
        },
        {
            "title" : "Compras",
            "icon" : "timeline",
            "subs" : [
                {
                    "title" : "Solicitudes",
                    "href" : "/solicitudesDeIsumos"
                },
                {
                    "title" : "Contizaciones en proceso",
                    "href" : "/quotes",
                    "simple" : true
                },
                {
                    "title" : "Categorias",
                    "href" : "/categoriesShopping",
                    "simple" : true
                },
                {
                    "title" : "Proveedores",
                    "href" : "/providers",
                    "simple" : true
                },
            ]
        },
        {
            "title" : "Administración",
            "icon" : "timeline",
            "subs" : [
                {
                    "title" : "Años",
                    "href" : "/years"
                },
                {
                    "title" : "Usuarios",
                    "href" : "/users"
                },
                {
                    "title" : "Areas",
                    "href" : "/areas"
                },
                {
                    "title" : "Oficinas",
                    "href" : "/offices"
                },
            ]
        }
    ]
}
