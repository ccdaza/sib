import Vue from 'vue';
import Router from 'vue-router';
import middleware from "./middleware";
import Login from './pages/Login';
import Home from './pages/Home';
import Profile from './pages/Profile';
import CompetitivenessMatrix from './pages/CompetitivenessMatrix';
import Pestel from './pages/Pestel';
import Dofa from './pages/Dofa';
import DofaScore from './pages/DofaScore';
import StrategySummary from './pages/StrategySummary';
import InterestGroups from './pages/InterestGroups';
import StrategicIssuesMatrix from './pages/StrategicIssuesMatrix';
import StrategicIssuesMatrixScore from './pages/StrategicIssuesMatrixScore';
import StrategicIssues from './pages/StrategicIssues';
import BalancedScorecard from './pages/BalancedScorecard';
import Budget from './pages/Budget';
import ApproveBudget from './pages/ApproveBudget'
import ApprovedBudget from './pages/ApprovedBudget'
import CashFlowRequests from "./pages/CashFlowRequests";
import Years from "./pages/Years";
import Users from "./pages/Users";
import Areas from "./pages/Areas";
import Offices from "./pages/Offices";
import SetAccounts from "./pages/SetAccounts";
import RequestSupplies from "./pages/RequestSupplies";
import RequestSuppliesShopping from "./pages/RequestSuppliesShopping";

require('bootstrap');

Vue.use(Router);


export default new Router({
    routes: [
        {
            path: '/',
            name: 'Login',
            component: Login,
            params: {
                showMenu: false
            },
            beforeEnter: (to, from, next) => {
                if(localStorage.autenticate_token){
                    next({ name: "Home" });
                }else{
                    next()
                }
            }
        },
        {
            path: '/home',
            name: 'Home',
            component: Home,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/profile',
            name: 'Perfil',
            component: Profile,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/competitivenessMatrix',
            name: 'Matriz perfil de competitividad',
            component: CompetitivenessMatrix,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/pestel',
            name: 'Pestel',
            component: Pestel,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/dofa',
            name: 'Dofa',
            component: Dofa,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/dofaScore',
            name: 'Dofa Calificación',
            component: DofaScore,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/strategySummary',
            name: 'Resumen de estrategias',
            component: StrategySummary,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/interestGroups',
            name: 'Grupos de interes',
            component: InterestGroups,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/strategicIssuesMatrix',
            name: "Matriz de cobertura de los temas estrategicos",
            component: StrategicIssuesMatrix,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/strategicIssuesMatrixScore',
            name: "Matriz de cobertura de los temas estrategicos Calificacion",
            component: StrategicIssuesMatrixScore,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/strategicIssues',
            name: "Temas estrategicos",
            component: StrategicIssues,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/balancedScorecard',
            name: "Cuadro de mando integral",
            component: BalancedScorecard,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/budget/request',
            name: "Solicitud de presupuesto",
            component: Budget,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/budget/approve',
            name: "Aprovación de presupuesto",
            component: ApproveBudget,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/budget/approved',
            name: "Presupuesto aprobado",
            component: ApprovedBudget,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/cashFlowRequest',
            name: "Solicitud de flujo de caja",
            component: CashFlowRequests,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/years',
            name: "Años",
            component: Years,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/users',
            name: "Usuarios",
            component: Users,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/setAccounts',
            name: "Asignacion de cuentas",
            component: SetAccounts,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/areas',
            name: "Areas",
            component: Areas,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/offices',
            name: "Offices",
            component: Offices,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/solicitarInsumos/:activity',
            name: "Solicitud de insusmos",
            component: RequestSupplies,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        },
        {
            path: '/solicitudesDeIsumos',
            name: "Solicitudes de insusmos",
            component: RequestSuppliesShopping,
            params: {
                showMenu: true
            },
            beforeEnter: middleware.auth
        }
    ]
})
