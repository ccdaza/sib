@extends("layouts.app")
@section("content")
    <index>
        <template v-slot:content>
            <div class="container py-4">
                <div class="row">
                    <div class="col-9">
                        <h3 class="title">{{ $provider->name }}</h3>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route("providers.updateFiles", $provider->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row mb-3">
                                <div class="col-6">
                                    <label for="rut">RUT</label>
                                    <label for="rutFile" data-type="upload" @if(!is_null($provider->rut)) data-file_name="<a target='_blank' href='/providers/rut/{{ $provider->id }}' >RUT</a>" @endif>
                                        <input type="file" id="rutFile" name="rut" accept="application/pdf" class="form-control" placeholder="Rut" required>
                                    </label>
                                </div>
                                <div class="col-6">
                                    <label for="camara">Camara de comercio</label>
                                    <label for="camaraFile"  data-type="upload" @if(!is_null($provider->camara)) data-file_name="<a target='_blank' href='/providers/camara/{{ $provider->id }}' >CAMARA</a>" @endif>
                                        <input type="file" id="camaraFile" name="camara" accept="application/pdf" class="form-control" placeholder="Camara de comercio" required>
                                    </label>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12 text-right">
                                    <button type="submit" class="btn btn-red">
                                        Subir documentos
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </template>
    </index>
@endsection

@section("script")
    <script src="{{ asset("js/uploadFile.js") }}"></script>
@endsection
