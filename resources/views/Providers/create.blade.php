@extends("layouts.app")
@section("content")
    <index>
        <template v-slot:content>
            <div class="container py-4">
                <div class="row">
                    <div class="col-9">
                        <h3 class="title">Nuevo proveedor</h3>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route("providers.store") }}" method="POST">
                            @csrf
                            <div class="row mb-3">
                                <div class="col-6">
                                    <input type="text" name="name" class="form-control" placeholder="Nombre" required>
                                </div>
                                <div class="col-6">
                                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-6">
                                    <input type="text" name="adviser" class="form-control" placeholder="Nombre contacto" required>
                                </div>
                                <div class="col-6">
                                    <input type="text" name="phone" class="form-control" placeholder="Teléfono" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-6">
                                    <input type="text" name="nit" class="form-control" placeholder="NIT">
                                </div>
                                <div class="col-6">
                                    <input type="text" name="direccion" class="form-control" placeholder="Dirección">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-6">
                                    <input type="text" name="city" class="form-control" placeholder="Ciudad">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12 text-right">
                                    <button type="submit" class="btn btn-red">
                                        Crear proveedor
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </template>
    </index>
@endsection
