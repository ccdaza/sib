@extends("layouts.app")
@section("content")
    <index>
        <template v-slot:content>
            <div class="container py-4">
                <div class="row">
                    <div class="col-9">
                        <h3 class="title">{{ $provider->name }}</h3>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route("providers.update", $provider->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="row mb-3">
                                <div class="col-6">
                                    <input type="text" name="name" class="form-control" placeholder="Nombre" value="{{ $provider->name }}" required>
                                </div>
                                <div class="col-6">
                                    <input type="email" name="email" class="form-control" placeholder="Email" value="{{ $provider->email }}" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-6">
                                    <input type="text" name="adviser" class="form-control" placeholder="Nombre contacto" value="{{ $provider->adviser }}" required>
                                </div>
                                <div class="col-6">
                                    <input type="text" name="phone" class="form-control" placeholder="Teléfono" value="{{ $provider->phone }}" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-6">
                                    <input type="text" name="nit" class="form-control" placeholder="NIT" value="{{ $provider->nit }}">
                                </div>
                                <div class="col-6">
                                    <input type="text" name="direccion" class="form-control" placeholder="Dirección" value="{{ $provider->direccion }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-6">
                                    <input type="text" name="city" class="form-control" placeholder="Ciudad"  value="{{ $provider->ciudad }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12 text-right">
                                    <button type="submit" class="btn btn-red">
                                        Editar proveedor
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </template>
    </index>
@endsection
