@extends("layouts.appSimple")
@section("content")
    <div class="container py-4">
        <div class="row">
            <div class="col-9">
                <h3 class="title">Bienvenido {{ $provider->name }}</h3>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <form action="{{ route("providers.updateFiles2", $provider->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row mb-3">
                        <div class="col-6">
                            <label for="rut">RUT</label>
                            <input type="file" id="rut" name="rut" accept="application/pdf" class="form-control" placeholder="Rut" required>
                        </div>
                        <div class="col-6">
                            <label for="camara">Camara de comercio</label>
                            <input type="file" id="camara" name="camara" accept="application/pdf" class="form-control" placeholder="Camara de comercio" required>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-red">
                                Subir documentos
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
