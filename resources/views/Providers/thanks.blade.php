@extends("layouts.appSimple")
@section("content")
    <div class="container py-4">
        <div class="row">
            <div class="col-9">
                <h3 class="title">Gracias {{ $provider->name }}</h3>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
               <h4>Muchas gracias por enviarnos tu documentación pronto nos contactaremos contigo</h4>
            </div>
        </div>
    </div>
@endsection
