@extends("layouts.app")
@section("content")
    <index>
        <template v-slot:content>
            <div class="container py-4">
                <div class="row">
                    <div class="col-9">
                        <h3 class="title">Proveedores</h3>
                    </div>
                    <div class="col text-right">
                        <a href="{{ route("providers.create") }}" class="btn btn-red">
                            Nuevo proveedor
                        </a>
                    </div>
                </div>
                <div class="card">
                    <div class="card-head pt-4">

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Email</th>
                                    <th>Contacto</th>
                                    <th>Telefono</th>
                                    <th>Prospecto</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($providers as $provider)
                                    <tr>
                                        <td>{{ $provider->name }}</td>
                                        <td>{{ $provider->email }}</td>
                                        <td>{{ $provider->adviser }}</td>
                                        <td>{{ $provider->phone }}</td>
                                        <td>
                                            <div class="alert {{$provider->state ? "alert-success" : "alert-danger"}} text-center">
                                                {{ $provider->state ? "Proveedor" : "Prospecto" }}
                                            </div>
                                        </td>
                                        <td>
                                            <a href="{{ route("providers.finalizar", $provider->id) }}" class="btn btn-red w-100 mb-2">
                                                Documentos
                                            </a>
                                            <a href="{{ route("providers.show", $provider->id) }}" class="btn btn-red w-100">
                                                Editar
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </template>
    </index>
@endsection
