@extends("mail.layout")

@section("body")
    <p style="text-align: center; padding: 20px;">
        Usted ha sido seleccionado para contratar como proveedor del Colegio San Bartolome.
        <br>
        Por favor suba su rut y camara de comercio en el siguiente link
    </p>
    <p style="text-align: center;">
        <a href="{{ route("providers.updateFiles.link", $hash) }}" target="_blank" style="background-color: #ff0125;color: #fff;padding: 10px;width: 200px;display: inline-block;">
            Enviar Documentos
        </a>
    </p>
@endsection
