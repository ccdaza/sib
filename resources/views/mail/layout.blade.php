<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body style="background-color: #ecf3f5">
<div style="max-width: 640px;position: relative;margin: auto;min-height: 400px;background-color: #fff;">
    <p style="padding: 10px; text-align: center">
        <img src="{{ env("APP_URL") }}/logo.png" alt="Logo {{ env("APP_NAME") }}" style="width: 80px">
    </p>
    <div style="background-color: rgb(255, 1, 37);">
        <h2 style="color: #fff; padding: 10px; text-align: center">{{ env("APP_NAME") }}</h2>
    </div>
    <div style="min-height: 150px; border-bottom: 1px solid #b6b6b7">
        @yield("body")
    </div>
    <div style="background-color: rgb(255, 1, 37);min-height: 80px; color:#fff">
        @yield("footer")
    </div>
</div>
</body>
</html>
