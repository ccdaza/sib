@extends("layouts.app")
@section("content")
    <index>
        <template v-slot:content>
            <div class="container py-4">
                <div class="row">
                    <div class="col-12">
                        <h3 class="title">Crear categoria <a href="{{ route("categoriesShopping.index") }}" class="btn btn-red float-right">Volver</a></h3>
                    </div>
                </div>
                <div class="card">
                    <div class="card-head pt-4">

                    </div>
                    <div class="card-body">
                        <form action="{{ route("categoriesShopping.store") }}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    <div class="input-group">
                                        <input type="text" name="name" class="form-control" placeholder="Nombre de la categoria">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-red">Crear</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </template>
    </index>
@endsection
