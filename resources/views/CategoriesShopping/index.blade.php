@extends("layouts.app")
@section("content")
    <index>
        <template v-slot:content>
            <div class="container py-4">
                <div class="row">
                    <div class="col-12">
                        <h3 class="title">Categorias <a href="{{ route("categoriesShopping.create") }}" class="btn btn-red float-right">Nueva categoria</a></h3>
                    </div>
                </div>
                <div class="card">
                    <div class="card-head pt-4">

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($categories as $category)
                                        <tr>
                                            <td>{{$category->name}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </template>
    </index>
@endsection
