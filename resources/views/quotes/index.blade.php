@extends("layouts.app")
@section("content")

    <index>
        <template v-slot:content>
            <div class="container py-4">
                <div class="row">
                    <div class="col-12">
                        <h3 class="title">Cotizaciones en proceso</h3>
                    </div>
                </div>
                <div class="card">
                    <div class="card-head px-3 pt-4">
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="cotizaciones">
                                <thead>
                                <tr>
                                    <th rowspan="2">Area</th>
                                    <th rowspan="2">Fecha</th>
                                    <th rowspan="2">N° cotización</th>
                                    <th rowspan="2">Descripción</th>
                                    <th rowspan="2">Catidad</th>
                                    <th colspan="4">Provedor 1</th>
                                    <th colspan="4">Provedor 2</th>
                                    <th colspan="4">Provedor 3</th>
                                    <th rowspan="2">Proveedor aprobado</th>
                                    <th rowspan="2">Acciones</th>
                                </tr>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Valor</th>
                                    <th>Carac.</th>
                                    <th>C.V</th>
                                    <th>Nombre</th>
                                    <th>Valor</th>
                                    <th>Carac.</th>
                                    <th>C.V</th>
                                    <th>Nombre</th>
                                    <th>Valor</th>
                                    <th>Carac.</th>
                                    <th>C.V</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($quotes as $quote)
                                    <tr>
                                        <td>
                                            <div style="width: 150px">{{ $quote->supply->resource->activity->office->area->name }}</div>
                                        </td>
                                        <td>
                                            <div style="width: 150px">{{ $quote->created_at }}</div>
                                        </td>
                                        <td>
                                            <div style="width: 50px">{{ $quote->id }}</div>
                                        </td>
                                        <td>
                                            <div style="width: 150px">{{ $quote->supply->resource->description }}</div>
                                        </td>
                                        <td>
                                            <div style="width: 50px">{{ $quote->supply->resource->quantity }}</div>
                                        </td>
                                        <td>
                                            <div style="width: 150px">
                                                <select class="provider form-control" data-quote="{{ $quote->id }}" data-provider="1">
                                                    @if($quote->provider1)
                                                        <option value="{{$quote->provider1_id}}" selected>{{  $quote->provider1->name }}</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width: 150px">
                                                <input-interactive
                                                    icon="attach_money"
                                                    type="number"
                                                    data-provider="1"
                                                    value-default="{{ $quote->provider1_ratings ? $quote->provider1_ratings->valor : '' }}"
                                                    url="/api/quotes/{{ $quote->id }}/setValor/1"></input-interactive>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width: 150px">
                                                <input-interactive type="text" value-default="{{ $quote->provider1_ratings ? $quote->provider1_ratings->carac : '' }}" url="/api/quotes/{{ $quote->id }}/setCarac/1"></input-interactive>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width: 50px" class="position-relative">
                                                <input-interactive
                                                    type="number"
                                                    data-provider="1"
                                                    value-default="{{ $quote->provider1_ratings ? $quote->provider1_ratings->cv : '' }}"
                                                    url="/api/quotes/{{ $quote->id }}/setCV/1"></input-interactive>
                                                <span style="position: absolute;top: 55%;right: 5%;font-size: 10px;font-weight: bold;">Días</span>
                                            </div>
                                        </td>

                                        <td>
                                            <div style="width: 150px">
                                                <select class="provider form-control" data-quote="{{ $quote->id }}" data-provider="2">
                                                    @if($quote->provider2)
                                                        <option value="{{$quote->provider2_id}}" selected>{{  $quote->provider2->name }}</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width: 150px">
                                                <input-interactive
                                                    icon="attach_money"
                                                    type="number"
                                                    data-provider="2"
                                                    value-default="{{ $quote->provider2_ratings ? $quote->provider2_ratings->valor : '' }}"
                                                    url="/api/quotes/{{ $quote->id }}/setValor/2"></input-interactive>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width: 150px">
                                                <input-interactive
                                                    type="text"
                                                    data-provider="2"
                                                    value-default="{{ $quote->provider2_ratings ? $quote->provider2_ratings->carac : '' }}"
                                                    url="/api/quotes/{{ $quote->id }}/setCarac/2"></input-interactive>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width: 50px" class="position-relative">
                                                <input-interactive
                                                    type="number"
                                                    data-provider="2"
                                                    value-default="{{ $quote->provider2_ratings ? $quote->provider2_ratings->cv : '' }}"
                                                    url="/api/quotes/{{ $quote->id }}/setCV/2"></input-interactive>
                                                <span style="position: absolute;top: 55%;right: 5%;font-size: 10px;font-weight: bold;">Días</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width: 150px">
                                                <select class="provider form-control" data-quote="{{ $quote->id }}" data-provider="3">
                                                    @if($quote->provider3)
                                                        <option value="{{$quote->provider3_id}}" selected>{{  $quote->provider3->name }}</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width: 150px">
                                                <input-interactive
                                                    icon="attach_money" type="number" value-default="{{ $quote->provider3_ratings ? $quote->provider3_ratings->valor : '' }}" url="/api/quotes/{{ $quote->id }}/setValor/3"></input-interactive>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width: 150px">
                                                <input-interactive type="text" value-default="{{ $quote->provider3_ratings ? $quote->provider3_ratings->carac : '' }}" url="/api/quotes/{{ $quote->id }}/setCarac/3"></input-interactive>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width: 50px" class="position-relative">
                                                <input-interactive type="number" value-default="{{ $quote->provider3_ratings ? $quote->provider3_ratings->cv : '' }}" url="/api/quotes/{{ $quote->id }}/setCV/3"></input-interactive>
                                                <span style="position: absolute;top: 55%;right: 5%;font-size: 10px;font-weight: bold;">Días</span>
                                            </div>
                                        </td>

                                        <td>
                                            <div style="width: 200px">
                                                <input-interactive class="proveedorAprobado" type="select"
                                                                   url="/api/quotes/{{ $quote->id }}/setWinner"
                                                                   value-default="{{ $quote->provider3_ratings ? $quote->winner : '' }}"
                                                                   label="Seleccione el ganador"
                                                                   :items="[{value: 1, label: 'Proveedor 1'},{value: 2, label: 'Proveedor 2'},{value: 3, label: 'Proveedor 3'}]"
                                                ></input-interactive>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width: 150px">
                                                <a class="btn btn-success finalizar @if($quote->winner == '') d-none @endif" href="{{ route('finishQuote', $quote->id) }}">Finalizar proceso</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </template>
    </index>
@endsection
@section("script")
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        jQuery(document).ready(function(){
            jQuery('.provider').each(function(){
                jQuery(this).select2({
                    ajax: {
                        url: "/api/providers",
                        dataType: 'json'
                    }
                });
                jQuery(this).on('select2:select', function (e) {
                    let quoteId = jQuery(this).data("quote");
                    let provider = jQuery(this).data("provider");
                    var data = e.params.data;
                    jQuery.ajax({
                        url: "/api/quotes/" + quoteId + "/setProvider/" + provider,
                        method: "post",
                        data: {
                            "category" : data.id
                        }
                    })
                });
            })
            jQuery("#cotizaciones tr td input, #cotizaciones tr td select").on("change", function(){
                let parents = jQuery(this).parents();
                let parentV = jQuery(this).parent();
                let parent = null;
                for(let i in parents){
                    if(jQuery(parents[i]).prop("tagName") == "TR"){
                        parent = jQuery(parents[i]);
                        break;
                    }
                }

                empty = 0;
                parent.find("select[data-provider='1'], div[data-provider='1'] input, select[ data-provider='2'], div[data-provider='2'] input").each(function(){
                    if(jQuery(this).val() == ""){
                        empty += 1;
                    }
                })

                if(parentV.hasClass("proveedorAprobado")){
                    if(parent.find(".proveedorAprobado select").val() != ''){
                        parent.find(".finalizar").removeClass("d-none");
                    }else{
                        parent.find(".finalizar").addClass("d-none");
                    }
                    return;
                }

                if(empty == 0){
                    parent.find(".proveedorAprobado").removeClass("d-none");
                }else{
                    parent.find(".proveedorAprobado").addClass("d-none");
                }
            })

            jQuery("#cotizaciones tr td input:first-child").trigger("change")
        })
    </script>
@endsection
