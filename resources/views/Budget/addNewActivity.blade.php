@extends("layouts.app")
@section("content")
    <index>
        <template v-slot:content>
            <div class="container py-4">
                <div class="row">
                    <div class="col-9">
                        <h3 class="title">Adicción a presupuesto - Nueva actividad</h3>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <label for="ref">REF</label>
                                <input type="text" name="ref" id="ref" class="form-control">
                            </div>
                            <div class="col-6">
                                <label for="uee">UEE</label>
                                <select name="uee" id="uee" class="form-control">
                                    @foreach($areas as $area)
                                        <option value="{{$area->id}}">{{$area->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <add-new-activity-extra></add-new-activity-extra>
                        </div>
                    </div>
                </div>
            </div>
        </template>
    </index>
@endsection
