@extends("layouts.app")
@section("content")
    <index>
        <template v-slot:content>
            <div class="container py-4">
                <div class="row">
                    <div class="col-9">
                        <h3 class="title">Adicción a presupuesto</h3>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-6">
                                <a href="{{ route("budget.addNewActivity") }}" class="btn btn-success w-100 my-3 rounded-pill">
                                    NUEVA ACTIVIDAD
                                </a>
                                <a href="{{ route("UnapprovedActivities") }}" class="btn btn-dark-blue w-100 my-3 rounded-pill">
                                    ACTIVIDADES NO APROBADAS
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </template>
    </index>
@endsection
