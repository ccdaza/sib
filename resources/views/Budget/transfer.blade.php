@extends("layouts.app")
@section("content")
    <index>
        <template v-slot:content>

            <div class="container py-4">

                @if(Session::has('success'))
                    <p class="alert alert-success">{{ Session::get('success') }}</p>
                @endif
                <div class="row">
                    <div class="col-9">
                        <h3 class="title">Translado de presupuesto</h3>
                    </div>
                </div>
                    <div class="card">
                        <div class="card-body">

                            <form action="" method="post">
                                @csrf
                                <div class="row justify-content-center">
                                    <div class="col-12 col-md-4">
                                        <label for="area">Área</label>
                                        <select name="area" id="area" class="form-control select2" required>
                                            <option value=""></option>
                                            @foreach(\App\Area::all() as $area)
                                                <option value="{{ $area->id }}">{{ $area->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <label for="accion">Acción</label>
                                        <input autocomplete="off" type="text" class="form-control" name="accion" id="accion" required>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <label for="area">Ejecutor</label>
                                        <select name="ejecutor" id="ejecutor" class="form-control select2" required>
                                            <option value=""></option>
                                            @foreach(\App\User::all() as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row justify-content-center mt-3">
                                    <div class="col-12 col-md-4">
                                        <label for="valor">Valor</label>
                                        <input autocomplete="off" type="number" id="valor" class="form-control" name="valor" placeholder="Valor" required>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <label for="areaSaldo">Área de saldo</label>
                                        <select name="areaSaldo" id="areaSaldo" class="form-control select2" required>
                                            <option value=""></option>
                                            @foreach(\App\Area::all() as $area)
                                                <option value="{{ $area->id }}">{{ $area->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <br>
                                        <button type="submit" class="btn btn-red w-100">Crear tranferencia</button>
                                    </div>
                                </div>
                            </form>
                            <div class="row mt-3">
                                <div class="col-12">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Área</th>
                                            <th>Acción</th>
                                            <th>Ejecutor</th>
                                            <th>Valor</th>
                                            <th>Área de saldo</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($transfers as $transfer)
                                            <tr>
                                                <td>{{ $transfer->area->name }}</td>
                                                <td>{{ $transfer->accion }}</td>
                                                <td>{{ $transfer->ejecutor->name }}</td>
                                                <td>{{ $transfer->valor }}</td>
                                                <td>{{ $transfer->areaSaldo->name }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </template>
    </index>
@endsection

@section("script")
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        jQuery(document).ready(function(){
            jQuery(".select2").select2();
        })
    </script>
@endsection
