<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        let queryString = window.location.search;
        let urlParams = new URLSearchParams(queryString);
        let token = urlParams.get("token")
        jQuery.ajax({
            url: "/api/getUser",
            async: false,
            headers: {
                Authorization: "Bearer " + token
            },
            success: function (response){
                localStorage.user = JSON.stringify(response);
                localStorage.autenticate_token = token;
                localStorage.year = (new Date()).getFullYear();
                window.location.replace("/");
            },
            error: function (error){
                alert("no fue posible")
            }
        })
    </script>
</body>
</html>
