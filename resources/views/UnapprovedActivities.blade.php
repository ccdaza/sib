@extends("layouts.app")
@section("content")
    <index>
        <template v-slot:content>
            <div class="container py-4">
                <div class="row">
                    <div class="col-9">
                        <h3 class="title">Actividades no aprovadas</h3>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Actividad</th>
                                        <th>Monto</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($activities as $activity)
                                    <tr>
                                        <td>{{ $activity->activity->action }}</td>
                                        <td>{{ $activity->activity->resources->reduce(function ($carry, $item){
                                                    return $carry + ($item->quantity * $item->value);
                                                })
                                            }}
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </template>
    </index>
@endsection
